Todos:
- Zeitschaltuhr auf Tage begrenzen

Infos:
- Gesamter Stromverbrauch (LEDs + ESP) bei 5 V bei maximaler Helligkeit ca. 1 A

  Windmühle:
    Bisher:
      Versorgung 12V~ +
      Widerstand: Rot Rot Braun Gold = 220 Ohm

    Nachher:
      Versorgung 5V
      Vorwiderstand keiner (PWM) oder ca. 100 Ohm
      Stromverbrauch mit 100 Ohm / ohne Widerstand ca. 3 bis 5 mA


  Innenraumhöhe: 27 mm
  Durchmesser Lampenlöcher: 10 mm
  Bodenplattenstärke: 11,5 mm
  Hausbodenplattenstärke: 5 mm (Hausnummer 3)

Pinbelegung:
  Schalter Zeitschaltuhr (oberer): 26, 27
  Schalter Windmühle (unterer): 33, 25
    (auf Aus funktioniert es, bei den anderen beiden war es wie ein Wackelkontakt)
  Schalter Lichttest (hinterer): 14
    (Wackelkontakt)
  Bewegungsmelder: Vermutlich 34
  Windmühle: 32
  LED-Stripe: 9
