from subprocess import run, PIPE
from datetime import datetime
import re

Import("env")

print("generate.py started")


def add_define(name: str, show_time: bool) -> None:
    ver = commit_hash
    options = []
    if branch_name:
        options += [branch_name]
    else:
        options += ["detached HEAD"]
    options += tag_names
    if has_modified_files:
        options += ["modified"]
    if show_time:
        options += [datetime.utcnow().strftime("%Y-%m-%d %H:%MZ")]
    options = ", ".join(options)
    if options:
        ver = f"{ver} ({options})"
    header.add_define_value(name, ver)


result = run(["git", "rev-parse", "HEAD"], stdout=PIPE, stderr=PIPE)
commit_hash = result.stdout.decode("ascii")[:7]

result = run(["git", "tag", "--points-at"], stdout=PIPE, stderr=PIPE)
tag_names = result.stdout.decode("utf8").splitlines()

result = run(["git", "branch", "--show-current"], stdout=PIPE, stderr=PIPE)
if result.returncode == 0:
    branch_name = result.stdout.decode("utf8").strip()
else:
    # Older versions of "git", don't support "--show-current", it can parse it
    # manually from the list. Unfortunately detached branches are still present (but useless).
    result = run(["git", "branch"], stdout=PIPE, stderr=PIPE)
    branch_name = None
    for branch in result.stdout.decode("utf8").splitlines():
        if branch.startswith("* "):
            if not re.match(r"\* \(HEAD detached at [0-9a-f]{7}\)", branch):
                branch_name = branch[2:]
            break

result = run(["git", "status", "-s", "--", "data", "src"], stdout=PIPE, stderr=PIPE)
#result = run("git status -s -- data", stdout=PIPE, stderr=PIPE)
has_modified_files = bool(result.stdout)

class HeaderFile:

    def __init__(self):
        self._content = f"""
        // AUTO GENERATED FILE, DO NOT EDIT
        #pragma once
        """

    def add_define_value(self, name: str, value: str):
        self._content += f"""
        #ifndef {name}
            #define {name} "{value}"
        #endif
        """

    def add_define(self, name: str):
        self._content += f"""
        #ifndef {name}
            #define {name}
        #endif
        """

    @property
    def content(self):
        return self._content

header = HeaderFile()
if has_modified_files:
    header.add_define("GIT_MODIFIED_FILE")
header.add_define_value("GIT_REV", commit_hash)

add_define("VERSION", has_modified_files)
add_define("VERSION_TIMED", True)

with open("include/version.h", "w") as f:
  f.write(header.content)

# def generate_header(source, target, env):
  # print("PRE_ACTION")
  # pprint.pprint(source)
  # pprint.pprint(target)
  # pprint.pprint(env)
  # print("STOP PRE_ACTION")

# env.AddPreAction("buildprog", generate_header)
# env.AddPreAction("${BUILD_DIR}/src/webserver.cpp.o", generate_header)