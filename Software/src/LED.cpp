#include "LED.h"
#include <Arduino.h>
#include <stdint.h>
#include <FastLED.h>
#include "Classes.h"

namespace Configuration {
  namespace Mode {
    const uint8_t Shift = 0;
    const uint8_t All = (1 << 0) | (1 << 1);
  }
  const uint8_t Sylvester = 1 << 2;
  const uint8_t Debug = 1 << 3;
}

// Handler for swichting lights during test
TaskHandle_t LED_Control_handler;

/**
* @brief Construct a new led control object
*/
LED_Control::LED_Control() {
  RemoveColorAll();

  FastLED.addLeds<WS2812B, LED_PIN, GRB>(ledBuffer, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(*(storage->Brightness));

  Serial.printf(" Lampstatus codes:\n  Off: %d\n  Single: %d\n  CycleTime: %d\n  CycleSwitch: %d\n  Complete: %d\n", (uint8_t)Lampteststatus::Off, (uint8_t)Lampteststatus::Single, (uint8_t)Lampteststatus::CycleTime, (uint8_t)Lampteststatus::CycleSwitch, (uint8_t)Lampteststatus::Complete);

  const uint8_t configuration = *(storage->Switching_Configurations);
  mode = (LED_Control::Mode)(configuration & Configuration::Mode::All);
  sylvester = configuration & Configuration::Sylvester;
  debug = configuration & Configuration::Debug;

  Serial.printf(" Loaded switching times configuration:\n");
  Serial.printf("  Mode: %s\n", mode == Mode::Off ? "Off" : (mode == Mode::Auto ? "Auto" : "On"));
  Serial.printf("  Sylvester: %s\n", sylvester ? "True" : "False");
  Serial.printf("  Debug-Mode: %s\n", debug ? "On" : "Off");

  LoadColors();

  xTaskCreatePinnedToCore(CheckLEDs_static, "LED Control", 2000, this, 1, &LED_Control_handler, 1);
}

/**
 * @brief (De-)activates lamptest
 *
 * @param nexttype The requested lampstatus (Lampstatus::Off, Single, CycleTime, CycleSwitch, Complete)
 * @param new_testlamp Zero based index of the lamp to be tested, only neccessary for nexttype == Single
 */
void LED_Control::SwitchLampTest(const Lampteststatus nexttype, const uint16_t new_testlamp, const CRGB* color) {
  if (lampstatus != nexttype)
    Serial.printf("Wechsle Lampentest\n");

  lampstatus = nexttype;

  RemoveColorAll();

  switch (lampstatus) {
  case Lampteststatus::Off:
    break;
  case Lampteststatus::CycleTime:
    testlamp = 0;
    SetColor(testlamp);
    last_run = millis();
    break;
  case Lampteststatus::Single:
    if (new_testlamp < NUM_LEDS) {
      testlamp = new_testlamp;
      SetColor(testlamp, color);
    }
    break;
  case Lampteststatus::CycleSwitch:
    if (new_testlamp == 0) { // Start test
      testlamp = 0;
      SetColor(testlamp);
      Apply();
    } else if (testlamp >= 23) {
      SwitchLampTest(Lampteststatus::Off);
    } else {
      RemoveColor(testlamp);
      testlamp++;
      SetColor(testlamp);
      Apply();
    }
    break;
  case Lampteststatus::Complete:
    for (uint8_t i = 0; i < NUM_LEDS; i++)
      SetColor(i, color);
    last_run = millis();
    break;
  default:
    break;
  }

  Apply();
}

/**
 * @brief Returns the current test status
 * @retval Lampteststatus
 */
Lampteststatus LED_Control::Getlamptesttype() const {
  return lampstatus;
}

/**
 * @brief Returns if the light of the given zero based index is on
 *
 * @param index Zero based index of the testlamp
 * @retval true Light is on
 * @retval false Light is off
 */
bool LED_Control::GetLighton(const uint16_t index) const {
  if (index >= NUM_LEDS)
    return false;
  else {
    const CRGB color = *GetColor(index);
    return color.red | color.blue | color.green;
  }
}

void LED_Control::SetColor(CRGB* target, const CRGB source) {
  if (target == nullptr)
    return;
  target->red = source.red;
  target->green = source.green;
  target->blue = source.blue;
}

void LED_Control::SetColor(const uint16_t index) {
  SetColor(index, house_colors[index]);
}

void LED_Control::SetColor(const uint16_t target, const CRGB source) {
  SetColor(GetColor(target), source);
}

void LED_Control::SetColor(const uint16_t target, const CRGB* color) {
  if (color == nullptr) {
    SetColor(target);
  } else {
    SetColor(target, *color);
  }
}

void LED_Control::SetColor(const uint16_t target, const uint8_t R, const uint8_t G, const uint8_t B) {
  SetColor(target, CRGB(R, G, B));
}

void LED_Control::RemoveColor(const uint16_t target) {
  SetColor(target, CRGB(0, 0, 0));
}

void LED_Control::RemoveColorAll() {
  for (uint16_t i = 0; i < NUM_LEDS; i++)
    SetColor(i, CRGB(0, 0, 0));
}

const CRGB* LED_Control::GetHouseColors() {
  return house_colors;
}

const CRGB* LED_Control::GetColor(const uint16_t index) const {
  if (index > NUM_LEDS)
    return nullptr;
  return &(leds[LED_ORDER[index]]);
}

CRGB* LED_Control::GetColor(const uint16_t index) {
  if (index > NUM_LEDS)
    return nullptr;
  return &(leds[LED_ORDER[index]]);
}


/**
 * @brief Loads color from EEPROM
 */
void LED_Control::LoadColors() {
  const uint8_t* _house_colors = *(storage->House_Colors);
  for (uint16_t i = 0; i < NUM_LEDS; i++)
    house_colors[i] = CRGB(_house_colors[i * 3 + 0], _house_colors[i * 3 + 1], _house_colors[i * 3 + 2]);
}


/**
 * @brief Caller function for CheckSwitchingTimes
 *
 * @param _this SwitchingTimes object
 */
void LED_Control::CheckLEDs_static(void* _this) {
  ((LED_Control*)_this)->CheckLEDs();
}

void LED_Control::CheckLEDs() {
  while (1) {
    switch (lampstatus) {
    case Lampteststatus::Off:
      if (main_time.tm_mon == 11 || main_time.tm_mon == 0 || debug) {
        CheckSylvester(); // Blocking function!
        CalculateAdventLight();
        Apply();
      } else {
        RemoveColorAll();
        Apply();
      }
      break;
    case Lampteststatus::Single:
    case Lampteststatus::Complete:
    case Lampteststatus::CycleSwitch:
      break;
    case Lampteststatus::CycleTime:
      if (millis() - last_run > TEST_DURATION) {
        last_run = millis();
        RemoveColor(testlamp);
        testlamp++;
        if (testlamp > 23) {
          Serial.printf("Letztes Haus erreicht. Beende Lampentest\n");
          SwitchLampTest(Lampteststatus::Off);
        } else {
          SetColor(testlamp);
          taskYIELD();
          Apply();
        }
      }
      break;
    default:
      Serial.printf("Fehler - Unbekannter lampstatus: %d\n", (uint8_t)lampstatus);
      lampstatus = Lampteststatus::Off;
      break;
    }
    vTaskDelay(10 / portTICK_PERIOD_MS);
  }
}

void LED_Control::CheckSylvester() {
  if (sylvester && main_time.tm_sec > (60 - NUM_LEDS) && (debug || (main_time.tm_mon == 11 && main_time.tm_mday == 31 && main_time.tm_hour == 23 && main_time.tm_min == 59))) {
    for (int_fast16_t i = 0; i < NUM_LEDS; i++)
      SetColor(i);

    // Countdown (24 seconds)
    while (1) {
      const struct tm tim = main_time;
      for (int_fast16_t i = 0; i < tim.tm_sec - 36; i++)
        RemoveColor(i);
      Apply();

      vTaskDelay(50 / portTICK_PERIOD_MS);

      if (tim.tm_sec < 36)
        break;
    }

    // Midnight (5 seconds)        
    for (uint16_t j = 0; j < 50; j++) {
      for (uint16_t i = 0; i < NUM_LEDS; i++)
        SetColor(i);
      Apply();

      vTaskDelay(50 / portTICK_PERIOD_MS);

      RemoveColorAll();
      Apply();

      vTaskDelay(50 / portTICK_PERIOD_MS);
    }

    // Firework (10 minutes)
    uint32_t endtime = millis() + (debug ? 5000 : 600000);
    while (endtime > millis()) {
      bool explosion = (random8() % 9) == 0;

      for (uint16_t i = 0; i < NUM_LEDS; i++) {
        if (explosion)
          leds[LED_ORDER[i]] = CRGB(255, 255, 255);
        else
          leds[LED_ORDER[i]] = CRGB(random8() % 100, random8() % 100, random8() % 100);
      }
      Apply();

      vTaskDelay(50 / portTICK_PERIOD_MS);
    }

    for (uint16_t i = 0; i < NUM_LEDS; i++)
      leds[LED_ORDER[i]] = house_colors[i];
    Apply();
  } // Sylvester end
}

void LED_Control::CalculateAdventLight() {
  if (mode == Mode::On || (mode == Mode::Auto && CheckTime())) {
    if (debug) {
      for (uint16_t i = 0; i < NUM_LEDS; i++)
        if (i < (main_time.tm_min % NUM_LEDS))
          SetColor(i);
        else
          RemoveColor(i);
    } else {
      for (uint16_t i = 0; i < NUM_LEDS; i++)
        if ((i < main_time.tm_mday) || (main_time.tm_mon == 0))
          SetColor(i);
        else
          RemoveColor(i);
    }
  } else
    RemoveColorAll();
  Apply();
}

// Configurations

void LED_Control::SaveConfiguration() {
  uint8_t configuration = mode << Configuration::Mode::Shift;
  configuration += sylvester ? Configuration::Sylvester : 0;
  configuration += debug ? Configuration::Debug : 0;

  Serial.printf("Save configurations to:\n Mode: %s\n Sylvester: %s\n Debug: %s\n", mode == Mode::Off ? "Off" : (mode == Mode::Auto ? "Auto" : "On"), sylvester ? "True" : "False", debug ? "On" : "Off");
  storage->Switching_Configurations->Save(configuration);
}

LED_Control::Mode LED_Control::GetMode() const {
  return mode;
}

void LED_Control::SetMode(const LED_Control::Mode _mode) {
  mode = _mode;
}

void LED_Control::SetMode(const uint8_t _mode) {
  SetMode((LED_Control::Mode)_mode);
}

bool LED_Control::GetSylvester() const {
  return sylvester;
}

void LED_Control::SetSylvester(const bool _sylvester) {
  sylvester = _sylvester;
}

bool LED_Control::GetDebug() const {
  return debug;
}

void LED_Control::SetDebug(const bool _debug) {
  debug = _debug;
}

void LED_Control::SetBrightness(const uint8_t scale) {
  FastLED.setBrightness(scale);
  Apply();
}

void LED_Control::Apply() {
  /*
  This is a "workaround" for a weird issue we are facing:
  Some LEDs "block" the signal and the LEDs after them won't update properly/sometimes.
  It seems to work (better), when we clear it before hand and then show the output.
  In theory this method could just call FastLED.show() and everyone would be happy.
  */
  // first check if there has been a change, otherwise we do not need to do this to reduce flicker
  if (memcmp(ledBuffer, leds, NUM_LEDS * sizeof(CRGB)) != 0) {
    FastLED.clear(true);
    FastLED.clear(true);
    memcpy(ledBuffer, leds, NUM_LEDS * sizeof(CRGB));
    FastLED.show();
  }
}