/**
 * @file Storage.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-08-31
 *
 * Remarks:
 *          0x*0   0x*1   0x*2   0x*3   0x*4   0x*5   0x*6   0x*7   0x*8   0x*9   0x*A   0x*B   0x*C   0x*D   0x*E   0x*F
 * 0x00*  |                                                      WIFI SSID                                                -
 * 0x01*  -                                                      WIFI SSID                                                |
 * 0x02*  |                                                    WIFI Passwort                                              -
 *   -
 * 0x05*  -                                                    WIFI Passwort                                       | Haus 1 -
 * 0x06*  - Haus 2 ...                                               -                                                    -
 * 0x07*  -                                    ... Haus 24 |                           Schaltzeiten                       -
 * 0x08*  -                                                  Schaltzeiten                                                 -
 *   -
 * 0x0E*  -                                                                                                        | Schaltzeit Konfigurationen |
 * 0x0F*  | Haus 1 R G     B   / Haus 2 R G     B   /                           Hausfarben                                -
 *   -
 * 0x13*  -                            Haus 24 R G     B   | Hellig |                      Schalter
*/

#include "Storage.h"
#include <EEPROM.h>
#include "Classes.h"

/**
 * @brief Function called after resetting switching times configuration
*/
void Reset_SwitchingTimes() {
  switchingtimes->Load();
}

/**
 * @brief Function called after resetting led control configuration
*/
void Reset_LED_Control() {
  LED_control->LoadColors();
}

/**
 * @brief Function called after resetting switch configuration
*/
void Reset_Switch() {
  switches->Reset();
}

/**
 * @brief Construct a new Storage object
 */
Storage::Storage() {
  EEPROM.begin(EEPROM_SIZE);

  uint16_t startaddress = 0;

  static_assert(sizeof(Switches_Storage) <= LENGTH_PER_SWITCH, "Length for switch settings is too small");
  static_assert(sizeof(Windmill_Storage) <= LENGTH_WINDMILL, "Length for windmill is too small");

  for (uint16_t i = 0; i < OBJECTS_COUNT; i++) {
    objects[i].Init(startaddress);
    startaddress += objects[i].GetLength();
    if (startaddress >= EEPROM_SIZE) {
      Serial.printf("Warning! EEPROM-size too small!\n Requested address: %d\n Available size: %d\n Stop programm!\n", startaddress, EEPROM_SIZE);
      while (1)
        taskYIELD();
    }
  }
}


/**
 * @brief Resets a specific EEPROM to 0
 *
 * @param index First removed object
 */
void Storage::Storage::Reset(uint8_t index) {
  if (index < OBJECTS_COUNT)
    objects[index].Reset();
}

/**
 * @brief Resets the whole EEPROM to 0
 *
 * @param wifi_incl Wether it will also remove the WiFi information (SSID and Password)
 */
void Storage::Storage::ResetAll(bool wifi_incl) {
  for (uint8_t i = wifi_incl ? 0 : 2; i < OBJECTS_COUNT; i++)
    objects[i].Reset();
}


/**
 * @brief Construct a new storring object
 *
 * @param _length Length in bytes
 * @param _name Name
 * @param _type Type of the value (Type::*)
 * @param _default_type Type of the default value (Default_Type::*)
 * @param _reset_fun A function called after resetting
*/
Storage::Object::Object(uint16_t _length, const char* _name, Type _type, Default_Type _default_type, void(*_reset_fun)()) :
  length(_length), name(_name), type(_type), start(0), default_type(_default_type), value(new uint8_t[_length + (_type == Type::Text)]), reset_fun(_reset_fun) {}

/**
 * @brief Initializes the objects startaddress and loads its value. Function should not be called outside of Storage.cpp
 *
 * @param _start Startaddress in EEPROM
*/
void Storage::Object::Init(uint16_t _start) {
  start = _start;

  EEPROM.readBytes(start, value, length);

  if (type == Type::Text)
    value[length] = '\0';
}

/**
 * @brief Saves a value into EEPROM
 *
 * @param _value String value to be saved
 * @retval bool Return 0 if success, 1 if address is out of range
 */
bool Storage::Object::Save(String _value) {
  // TODO: In theory it shouldn't be necessary to memcpy here.
  //       But when just using c_str, everything after the string won't be zeroed-out.
  uint8_t buffer[length] = { 0 };
  memcpy(buffer, _value.c_str(), _value.length());
  return Save(buffer);
}

/**
 * @brief Saves a value into EEPROM
 *
 * @param _value One Byte value to be saved
 * @retval bool Return 0 if success, 1 if address is out of range
 */
bool Storage::Object::Save(uint8_t _value) {
  return Save((const uint8_t* const)&_value, 1);
}

/**
 * @brief Saves an object into EEPROM
 *
 * @param _value Object to be saved
 * @param _length Length of the value pointer
 * @param _offset Amount of bytes the start is shifted from the address
 * @retval bool Return 0 if success, 1 if address is out of range
*/
bool Storage::Object::Save(const void* _value, uint16_t _length, const uint16_t _offset) {
  return Save((const uint8_t*)_value, _length, _offset);
}

/**
 * @brief Saves a value into EEPROM
 *
 * @param _value uint8_t pointer to be saved
 * @param _length Length of the value pointer
 * @param _offset Amount of bytes the start is shifted from the address
 * @retval bool Return 0 if success, 1 if address is out of range
 */
bool Storage::Object::Save(const char* _value, uint16_t _length, const uint16_t _offset) {
  return Save((const uint8_t*)_value, _length, _offset);
}

/**
 * @brief Saves a value into EEPROM
 *
 * @param _value uint8_t pointer to be saved
 * @param _length Length of the value pointer
 * @param _offset Amount of bytes the start is shifted from the address
 * @retval bool Return 0 if success, 1 if address is out of range
 */
bool Storage::Object::Save(const uint8_t* _value, uint16_t _length, const uint16_t _offset) {
  uint16_t length_buf = _length;

  if (_length == (uint16_t)-1)
    length_buf = length;

  uint16_t i;
  Serial.printf("Storing: address=%u+%u=%u, length_buf=%u (%u)\nMessage: '", start, _offset, start + _offset, length_buf, length);
  for (i = 0; i < length_buf; i++)
    Serial.printf("%03u ", _value[i]);
  Serial.println("'");

  if (_offset + length_buf > length)
    length_buf = length - _offset;

  memcpy(value + _offset, _value, length_buf);
  EEPROM.writeBytes(start + _offset, _value, length_buf);
  EEPROM.commit();

  return 0;
}


/**
 * @brief Resets the object to its default value
*/
void Storage::Object::Reset() {
  Serial.printf("Resetting %s to its default value\n", name);

  switch (default_type) {
    break;
  case Default_Type::Max: {
    uint8_t val[length];
    for (uint16_t i = 0; i < length; i++)
      val[i] = 255;
    Save(val);
    break;
  }
  case Default_Type::Rising_Start_Zero: {
    uint8_t val[length];
    for (uint16_t i = 0; i < length; i++)
      val[i] = i;
    Save(val);
    break;
  }
  case Default_Type::Rising_Start_One: {
    uint8_t val[length];
    for (uint16_t i = 0; i < length; i++)
      val[i] = i + 1;
    Save(val);
    break;
  }
  case Default_Type::Individual: {
    break;
  }
  case Default_Type::Zero:
  default: {
    uint8_t val[length] = { 0 };
    Save(val);
    break;
  }
  }

  if (reset_fun != nullptr)
    reset_fun();
}


/**
 * @brief Get the length of the requested object
 *
 * @retval uint16_t Length of the requested object
*/
uint16_t Storage::Object::GetLength() const {
  return length;
}

/**
 * @brief Get the name of the object
 *
 * @retval const char* Name of the object
*/
const char* Storage::Object::GetName() const {
  return name;
}

/**
 * @brief Get the length of the requested object
 *
 * @retval uint16_t Length of the requested object
*/
Storage::Object::Type Storage::Object::GetType() const {
  return type;
}


/**
 * @brief Get the name of the ith object
 *
 * @param index Index of the object
 * @retval const char* Object's name
*/
const char* Storage::GetObjectsName(uint16_t i) {
  if (i < OBJECTS_COUNT)
    return objects[i].GetName();
  else
    return nullptr;
}

/**
 * @brief Get the ith object
 *
 * @param index Index of the object
 * @retval const Object* Requested object
*/
Storage::Object* Storage::GetObject(uint16_t i) {
  if (i < OBJECTS_COUNT)
    return objects + i;
  else
    return nullptr;
}
