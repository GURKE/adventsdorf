#include "webserver.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include "SPIFFS.h"            // https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/
#include "StaticFunctions.h"
#include "time.h"
#include "Switch.h"
#include "Classes.h"
#include "ArduinoOTA.h"

// NTP-Zeit
#define NTPSERVERNAME "de.pool.ntp.org"
#define GMTOFFSET_SEC 3600
#define DAYLIGHTOFFSET_SEC 3600

const uint8_t WAIT_TIME_SECOND_WIFI_TRY = 3; // in Seconds
const uint8_t WAIT_TIME_WIFI_AP = 20;        // in Seconds

AsyncWebServer server(80);

int32_t SignalStrength;
TaskHandle_t Signal_Strength_Handler;

template<typename Item, typename Container>
String createOptionList(Container&& list, const Item selected, const bool valueAsIndex) {
  String str;
  std::size_t index = 0;
  for (auto&& item : list) {
    str += "<option value=\"";
    if (valueAsIndex) {
      str += index;
    } else {
      str += item;
    }
    str += "\"";
    if (item == selected) {
      str += " selected=\"selected\"";
    }
    str += ">";
    str += item;
    str += "</option>\n";
    index++;
  }
  return str;
}

template<typename Container>
String createOptionList(Container&& list, const bool valueAsIndex) {
  return createOptionList(list, (const void*) nullptr, valueAsIndex);
}

uint16_t village_coords[LED_Control::NUM_LEDS][2] = { {84, 73}, {34, 7}, {6, 77}, {137, 5}, {1, 29}, {4, 54}, {127, 78}, {49, 73}, {74, 1}, {10, 7}, {24, 42}, {96, 2}, {135, 31}, {137, 57}, {102, 73}, {53, 2}, {107, 29}, {115, 3}, {66, 73}, {58, 51}, {87, 53}, {31, 72}, {119, 53}, {70, 30} };

AsyncStaticWebHandler& addStaticServe(const char* uri, const char* filename = nullptr) {
  if (filename == nullptr) {
    filename = uri;
  }
  return server.serveStatic(uri, SPIFFS, filename);
}

/**
 * @brief Checks whether there are enough arugments in request.
 *
 * In case the argument count is wrong it'll print a diagnostic text to Serial and sends a reply.
 *
 * @param request the request which will be checked
 * @param argCount the required argument count
 * @param function_name the function name which has it called
 * @retval Whether the required argument count is supplied.
 */
bool checkArgumentCount(AsyncWebServerRequest* request, const size_t argCount, const char* function_name = __builtin_FUNCTION()) {
  if (request->args() != argCount) {
    Serial.printf("%s: Invalid argument count: %u (required: %u, URL: '%s')\n", function_name, request->args(), argCount, request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
    return false;
  } else {
    return true;
  }
}

#define CHECK_ARGUMENT_COUNT(argCount) if (!checkArgumentCount(request, argCount)) {return;}

WIFI::WIFI() {
  // Mainpage
  server.on("/", HTTP_GET, HandleRoot);
  addStaticServe("/favicon.ico", "/favicon.png").setCacheControl("public, max-age=3600");
  addStaticServe("/scripts.js");
  addStaticServe("/style.css");

  // Lamptest
  server.on("/SingleLampTest", HTTP_GET, Handle_SingleTest);
  server.on("/CompleteLampTest", HTTP_GET, Handle_CompleteLampTest);
  server.on("/CycleLampTestTime", HTTP_GET, Handle_CycleLampTestTime);
  server.on("/CycleLampTestSwitch", HTTP_GET, Handle_CycleLampTestSwitch);
  server.on("/TestStop", HTTP_GET, Handle_StopTest);
  server.on("/lampstatus", HTTP_GET, [](AsyncWebServerRequest* request) {
    // three extra characters:
    //  * whether the lamp test is not off
    //  * whether the windmill is running
    //  * null-terminator
    char lampstatus[LED_Control::NUM_LEDS + 3];
    for (uint8_t i = 0; i < LED_Control::NUM_LEDS; i++)
      lampstatus[i] = LED_control->GetLighton(i) ? '1' : '0';
    lampstatus[LED_Control::NUM_LEDS + 0] = LED_control->Getlamptesttype() != Lampteststatus::Off ? '1' : '0';
    lampstatus[LED_Control::NUM_LEDS + 1] = windmill->running() ? '1' : '0';
    lampstatus[LED_Control::NUM_LEDS + 2] = '\0';
    request->send(200, "text/html", lampstatus);
  });

  // Switching Times
  server.on("/load_switch_time", HTTP_GET, Handle_GetSwitchingTimes);
  server.on("/save_switch_time", HTTP_GET, Handle_SetSwitchingTimes);

  // Colors
  server.on("/reset_house_colors", HTTP_GET, Handle_ResetHouseColors);
  server.on("/try_color", HTTP_GET, Handle_TryColor);
  server.on("/set_house_colors_single", HTTP_GET, Handle_SetHouseColorsSingle);
  server.on("/set_house_colors_common", HTTP_GET, Handle_SetHouseColorsCommon);
  server.on("/get_house_color", HTTP_GET, Handle_GetHouseColor);
  server.on("/save_color_configuration", HTTP_GET, Handle_SetColorConfigurations);

  // Settings
  server.on("/save_switch_configuration", HTTP_GET, Handle_SetSwitchingConfiguration);

  server.on("/new_order", HTTP_GET, Handle_NewHouseOrdner);
  server.on("/reset_house_pos", HTTP_GET, Handle_ResetHousePos);

  server.on("/load_configuration_variable", HTTP_GET, Handle_LoadConfigurationVariable);
  server.on("/save_configuration_variable", HTTP_GET, Handle_SaveConfigurationVariable);

  server.on("/reboot", HTTP_GET, [](AsyncWebServerRequest* request) {
    Serial.println("Restarting...");
    request->redirect("/");
    WiFi.disconnect(true);
    ESP.restart();
  });
  server.on("/reset", HTTP_GET, Handle_Reset);

  // Switches
  server.on("/load_switch_config", HTTP_GET, Handle_LoadSwitchConfiguration);
  server.on("/save_switch", HTTP_GET, Handle_SaveSwitch);

  server.on("/418", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(418, "text/plain", "418 I'm a teapot");
  });

  // Windmill
  server.on("/save_windmill_configuration", HTTP_GET, Handle_SaveWindmillConfiguration);

  server.onNotFound(HandleNotFound);

  xTaskCreatePinnedToCore(CalcSignalStrength, "Calc Signal Strength", 1000, NULL, 1, &Signal_Strength_Handler, 1);
}

void WIFI::OnWiFiEvent(WiFiEvent_t event) {
  switch (event) {
  case SYSTEM_EVENT_STA_CONNECTED:
    Serial.println("ESP32 Connected to WiFi Network");
    WiFi.enableAP(false);
    NetworkConnected();
    break;
  default: break;
  }
}

void WIFI::Check_Wifi() {
  ArduinoOTA.handle();
  static bool connected_once = false;

  if (WiFi.status() == WL_CONNECTED) {
    connected_once = true;
    if (!getLocalTime(&main_time)) {
      Serial.println("Failed to obtain time");
      return;
    }

    return;
  }

  if (WiFi.getMode() == WIFI_MODE_APSTA)
    return;

  digitalWrite(LED_BUILTIN, LOW);

  Serial.println("No WiFi connection, try to connect!");

  Serial.println("Loading Wifi credentials from EEPROM...");
  const char* wifi_ssid = *(storage->WiFi_SSID);
  const char* wifi_pass = *(storage->WiFi_PASS);

  Serial.printf("Loaded WIFI-credentials:\nSSID: '%s'\nPassword: '%s'\n", wifi_ssid, wifi_pass);
  WiFi.setHostname(HOSTNAME);
  WiFi.begin(wifi_ssid, wifi_pass);
  WiFi.reconnect();
  Serial.print("Connecting WIFI...");
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    vTaskDelay(500 / portTICK_PERIOD_MS);
    Serial.print(".");
    i++;
    if (i == WAIT_TIME_SECOND_WIFI_TRY * 3 || WiFi.status() == WL_CONNECT_FAILED) { // 2s gone -> 2nd try
      Serial.printf("\nCould not connect to WIFI (SSID: %s), Resetting WiFi-Connecting and try one more time\n", wifi_ssid);
      WiFi.disconnect(true);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      WiFi.begin(wifi_ssid, wifi_pass);
    } else if (i > WAIT_TIME_WIFI_AP * 2) { // Wifi totally failed!
      Serial.println("Check if connected once");
      if (connected_once) // Only start AP directly after boot
        return;
      Serial.println("Activate AP");

      WiFi.onEvent(OnWiFiEvent);
      WiFi.mode(WIFI_MODE_APSTA);

      WiFi.softAP(WIFI_AP_SSID, WIFI_AP_PASS);
      WiFi.begin(wifi_ssid, wifi_pass);

      server.begin();
      return;
    }
  }

  NetworkConnected();
}

void WIFI::NetworkConnected() {
  Serial.print("\nIP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());

  digitalWrite(LED_BUILTIN, HIGH);

  server.begin();
  SignalStrength = WiFi.RSSI();

  ArduinoOTA.setHostname(HOSTNAME);
  ArduinoOTA.setPassword(UPLOAD_AUTH);
  ArduinoOTA.begin();

  // Zeit
  configTime(GMTOFFSET_SEC, DAYLIGHTOFFSET_SEC, NTPSERVERNAME);
  if (!getLocalTime(&main_time)) {
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&main_time, "%A, %B %d %Y %H:%M:%S");

  Serial.printf("Adventsdorf zugreifbar über: %s.local\n", HOSTNAME);
}

uint8_t WIFI::GetSignalStrengthPercentage() {
  if (SignalStrength < -100)
    return 0;
  else if (SignalStrength > -50)
    return 100;
  else
    return (uint8_t)((SignalStrength + 100) * 2);
}

void WIFI::CalcSignalStrength(void* pvParameters) {
  while (1) {
    SignalStrength += WiFi.RSSI();
    SignalStrength /= 2;
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

// ============================== Handles ==============================
// ============================== General ==============================

void WIFI::HandleRoot(AsyncWebServerRequest* request) {
  request->send(SPIFFS, "/index.html", "text/html", false, HandleRoot_Placeholder);
}

String WIFI::HandleRoot_Placeholder(const String& var) {
  if (var == "VILLAGE") {
    const uint8_t* house_pos = *(storage->House_Positions);

    char* wp = new char[10000] {0};
    char* wp2 = wp;

    const char mainpage1[] PROGMEM = R"=====(
<div id="ziel%02d" class="target" ondrop="drop(event)" ondragover="allowDrop(event)" style="top: %dpx; left: %dpx;">
  <div id="Haus%02d" class="house%s" draggable="true" ondragstart="drag(event)" onclick="clickhouse(this)"><div><div></div></div><b>%d</b></div>
</div>
)=====";
    for (uint_fast8_t i = 1; i < 25; i++) {
      sprintf(wp2, mainpage1, i, village_coords[house_pos[i - 1]][1] * 5, village_coords[house_pos[i - 1]][0] * 5, i, LED_control->GetLighton(i) ? " house_on" : "", i);
      wp2 += strlen(wp2);
    }
    String str = String(wp);
    delete[](wp);
    return str;
  } else if (var == "SSID") {
    return WiFi.SSID();
  } else if (var == "WIFI_POWER") {
    char str[20];
    sprintf(str, "%u%%%% (%d dB)", GetSignalStrengthPercentage(), SignalStrength);
    return String(str);
  } else if (var == "CONFIGURATION_MODE") {
    LED_Control::Mode mode = LED_control->GetMode();
    const char* names[3] = { "Aus", "Auto", "An" };
    return createOptionList(names, names[mode], true);
  } else if (var == "WINDMILL_MODE") {
    uint8_t mode = (uint8_t)windmill->getMode();
    const char* names[4] = { "Aus", "Bewegung Zeitschaltung", "Bewegung", "An" };
    return createOptionList(names, names[mode], true);
  } else if (var == "CONFIGURATION_SYLVESTER") {
    return String(LED_control->GetSylvester() ? " checked" : "");
  } else if (var == "CONFIGURATION_DEBUG") {
    return String(LED_control->GetDebug() ? " checked" : "");
  } else if (var == "BRIGHTNESS") {
    return String((uint8_t) * (storage->Brightness));
  } else if (var == "BRIGHTNESS_P") {
    return String(*(storage->Brightness) * 100 / 255);
  } else if (var == "SWITCH_NAMES") {
    const std::array<const char*, Switches::AM_Switches> names = switches->GetNames();
    return createOptionList(names, true);
  } else if (var == "EEPROM_NAMES") {
    const char* names[storage->OBJECTS_COUNT];
    for (uint8_t i = 0; i < storage->OBJECTS_COUNT; i++) {
      const Storage::Object* obj = storage->GetObject(i);
      names[i] = obj->GetName();
    }
    return createOptionList(names, true);
  } else if (var == "SWITCH_FUNCTIONS") {
    const char* function_names[] = {
      "Keine",
      "Modus: Aus", "Modus: Auto", "Modus: An",
      "Lichttest beenden", "Lichttest: Komplett", "Lichttest: Zeitzyklus", "Lichttest: Klickzyklus",
      "Windmuehle: Aus", "Windmuehle: Bewegung", "Windmuehle: Bewegung Zeitschaltung", "Windmuehle: Ein" };
    const String functionNamesOptions = createOptionList(function_names, true);

    String str;
    for (uint8_t i = 0; i < Switches_Storage::AM_SWITCH_FUNS; i++) {
      str += "<tr id=\"switch_fun_" + String(i) + "_row\">\n";
      str += "<td><label for=\"switch_fun_" + String(i) + "\" id=\"switch_fun_" + String(i) + "_label\"></label></td>";
      str += "<td><select id=\"switch_fun_" + String(i) + "\" name=\"switch_fun_" + String(i) + "\">";
      str += functionNamesOptions;
      str += "</select></td>\n</tr>\n";
    }
    return str;
  } else if (var.substring(0, 4) == "PINS") {
    uint8_t selected = 0;
    if (var[4] == '1') {
      selected = windmill->GetPinWindmill();

      // 0, 34 and 35 are input only pins
      const uint8_t pinsOutput[] = { 2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 25, 26, 27, 32, 33 };
      return createOptionList(pinsOutput, selected, false);
    } else {
      if (var[4] == '2')
        selected = windmill->GetPinMotiondetector();

      // 0, 34 and 35 are special but theoretically connectable
      const uint8_t pins[] = { 2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 25, 26, 27, 32, 33, 34, 35 };
      return createOptionList(pins, selected, false);
    }
  } else if (var == "WM_LIVEWEATHER_ACTIVE") {
    return String(windmill->GetLiveWeatherActive() ? " checked" : "");
  } else if (var == "WINDMILL_WS_TRG") {
    return String(windmill->GetWindSpeedTrigger());
  } else if (var == "WINDMILL_CITY_ID") {
    return String(windmill->GetCityID()).substring(0, 7);
  } else if (var == "WINDMILL_APP_ID") {
    return String(windmill->GetAppID()).substring(0, 32);
  } else if (var == "WINDMILL_ACT_DUR") {
    return String(windmill->GetActionduration());
  } else if (var == "WINDMILL_SMODE") {
    return String((uint8_t)windmill->getSpeedMode());
  } else if (var == "WINDMILL_SC1") {
    return String(windmill->Get_Constant_Speed());
  } else if (var == "WINDMILL_SF1") {
    return String(windmill->Get_Function_Min());
  } else if (var == "WINDMILL_SF2") {
    return String(windmill->Get_Function_Max());
  } else if (var == "WINDMILL_SF3") {
    return String(windmill->Get_Function_Frequency());
  } else if (var == "WINDMILL_SL1") {
    return String(windmill->Get_Live_Min());
  } else if (var == "WINDMILL_SL2") {
    return String(windmill->Get_Live_Max());
  } else if (var == "WINDMILL_SL3") {
    return String(windmill->Get_Live_Turnoff());
  } else if (var == "IP") {
    return WiFi.localIP().toString();
  } else if (var == "VERSION") {
    return String(VERSION);
  } else if (var == "DATETIME") {
    char datetime[20];
    sprintf(datetime, "%02d.%02d.%04d %02d:%02d:%02d", main_time.tm_mday, main_time.tm_mon + 1, main_time.tm_year + 1900, main_time.tm_hour, main_time.tm_min, main_time.tm_sec);
    return String(datetime);
  }
  return String();
}

void WIFI::HandleNotFound(AsyncWebServerRequest* request) {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += request->url();
  message += "\nMethod: ";
  message += (request->method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += request->args();
  message += "\n";

  for (uint8_t i = 0; i < request->args(); i++) {
    message += " " + request->argName(i) + ": " + request->arg(i) + "\n";
  }

  request->send(404, "text/plain", message);
}


// ============================== Lamptests ==============================

void WIFI::Handle_CompleteLampTest(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::Complete);
  request->redirect("/");
}

void WIFI::Handle_CycleLampTestTime(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::CycleTime);
  request->redirect("/");
}

void WIFI::Handle_CycleLampTestSwitch(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::CycleSwitch, 0);
  request->redirect("/");
}

void WIFI::Handle_SingleTest(AsyncWebServerRequest* request) {
  if (request->args() > 0) {
    uint8_t lamp = parseAsInt<uint8_t>(request->arg(0U));
    lamp--;
    Serial.printf("Teste Lampe: %u\n", lamp + 1);
    LED_control->SwitchLampTest(Lampteststatus::Single, lamp); // Das richtige Haus aktivieren!
  } else
    LED_control->SwitchLampTest(Lampteststatus::Single, -1);
  request->redirect("/");
}

void WIFI::Handle_StopTest(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::Off);
  request->redirect("/");
}


// ============================== Switching Times ==============================

void WIFI::Handle_SetSwitchingConfiguration(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(3);
  const String& mode_str = request->arg(0U);
  const String& sylvester_str = request->arg(1U);
  const String& debug_str = request->arg(2U);

  LED_control->SetMode(parseAsInt<uint8_t>(mode_str, 0, 1));
  LED_control->SetSylvester(sylvester_str[0] == '1');
  LED_control->SetDebug(debug_str[0] == '1');

  LED_control->SaveConfiguration();

  request->send(200, "text/html", "0");
}

void WIFI::Handle_GetSwitchingTimes(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    const String& index_str = request->arg(0U);
    uint8_t index = parseAsInt<uint8_t>(index_str);

    SwitchingTimes::Time activate;
    SwitchingTimes::Time deactivate;
    uint8_t weekdays;
    bool enabled;

    if (switchingtimes->GetSwitchingTime(index, &activate, &deactivate, &weekdays, &enabled)) {
      char wp[30];
      sprintf(wp, "0 %02u:%02u-%02u:%02u %03u %u", activate.Hour, activate.Minute, deactivate.Hour, deactivate.Minute, weekdays, enabled);
      request->send(200, "text/html", wp);
    } else { // Index wrong
      request->send(200, "text/html", "Wrong index!");
    }
  }
}

void WIFI::Handle_SetSwitchingTimes(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(5);
  const String& index_str = request->arg(0U);
  const String& activate_str = request->arg(1U);
  const String& deactivate_str = request->arg(2U);
  const String& weekdays_str = request->arg(3U);
  const String& enabled_str = request->arg(4U);

  uint8_t index = parseAsInt<uint8_t>(index_str);
  uint8_t weekday = parseAsInt<uint8_t>(weekdays_str);

  switchingtimes->SetSwitchingTime(index, activate_str, deactivate_str, weekday, enabled_str[0] != '0');
  request->send(200, "text/html", "0");
}


// ============================== House Colors ==============================

void WIFI::Handle_TryColor(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(2);
  const String& lamp_str = request->arg(0U);
  const String& color_str = request->arg(1U);

  const CRGB color = parseAsColor(color_str);

  if (lamp_str[0] == 'a')
    LED_control->SwitchLampTest(Lampteststatus::Complete, -1, &color);
  else
    LED_control->SwitchLampTest(Lampteststatus::Single, parseAsInt<uint16_t>(lamp_str), &color); // Das richtige Haus aktivieren!

  request->send(200, "text/html", "0");
}

void WIFI::Handle_SetHouseColorsCommon(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(2);
  const String& color_str = request->arg(0U);
  const String& variance_str = request->arg(1U);

  const CRGB color = parseAsColor(color_str);

  uint8_t variance = parseAsInt<uint8_t>(variance_str);

  uint8_t colors[LED_Control::NUM_LEDS * 3];
  for (uint8_t i = 0; i < LED_Control::NUM_LEDS; i++) {
    for (uint8_t j = 0; j < 3; j++) {
      uint16_t index = i * 3 + j;
      int16_t new_color = color.raw[j] * (1 + variance / 100. * ((float)random8() - 128) / 128.);
      if (new_color > 255)
        colors[index] = 255;
      else if (new_color < 0)
        colors[index] = 0;
      else
        colors[index] = new_color;
    }
  }

  storage->House_Colors->Save(colors);
  LED_control->LoadColors();

  request->send(200, "text/html", "0");
}

void WIFI::Handle_SetHouseColorsSingle(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(2);
  const String& lamp_str = request->arg(0U);
  const String& color_str = request->arg(1U);

  uint8_t lamp = parseAsInt<uint8_t>(lamp_str);

  const CRGB color = parseAsColor(color_str);

  if (lamp > 23) {
    Serial.printf("Handle_SetHouseColorsSingle: Wrong lamp id: %u (URL: '%s')\n", lamp, request->url().c_str());
    request->send(200, "text/html", "Hausnummer nicht vergeben!");
    return;
  }
  storage->House_Colors->Save(color.raw, 3, 3 * lamp);
  LED_control->LoadColors();

  request->send(200, "text/html", "0");
}

void WIFI::Handle_GetHouseColor(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(1);
  const String& lamp_str = request->arg(0U);

  uint8_t lamp = parseAsInt<uint8_t>(lamp_str);

  if (lamp >= LED_Control::NUM_LEDS) {
    Serial.printf("Handle_GetHouseColor: Wrong lamp id: %u (URL: '%s')\n", lamp, request->url().c_str());
    request->send(200, "text/html", "Hausnummer nicht vergeben!");
    return;
  }

  const CRGB* color = LED_control->GetHouseColors() + lamp;
  char response[10];
  snprintf(response, 9, "0 %02x%02x%02x", color->r, color->g, color->b);
  request->send(200, "text/html", response);
}

void WIFI::Handle_SetColorConfigurations(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    const String& brightness_str = request->arg(0U);
    uint8_t brightness = parseAsInt<uint8_t>(brightness_str);

    storage->Brightness->Save(brightness);
    Serial.printf("Set Color Configurations:\nBrightness: %u\n", brightness);
    LED_control->SetBrightness(brightness);
    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_GetHouseColor: Too less arguments: %u/1 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_ResetHouseColors(AsyncWebServerRequest* request) {
  uint8_t colors[LED_Control::NUM_LEDS * 3];
  for (uint8_t i = 0; i < LED_Control::NUM_LEDS; i++) {
    colors[i * 3 + 0] = 255; // TODO generalize!
    colors[i * 3 + 1] = 221;
    colors[i * 3 + 2] = 104;
  }

  storage->House_Colors->Save(colors);
  LED_control->LoadColors();
  request->send(200, "text/html", "0");
}


// ============================== Configurations ==============================
void WIFI::Handle_NewHouseOrdner(AsyncWebServerRequest* request) {
  if (!request->hasArg("house_ids")) {
    Serial.println("house_ids == null!\n");
    request->send(200, "text/html", "house_ids = null!");
    return;
  } else if (!isNumber(request->arg("house_ids"))) {
    Serial.println("house_ids is not a number");
    request->send(200, "text/html", "house_ids is not a number!");
    return;
  }
  Serial.print("Arg: '");
  Serial.print(request->arg("house_ids"));
  Serial.print("'\n");
  const uint8_t* house_pos = *(storage->House_Positions);
  uint8_t new_house_pos[LED_Control::NUM_LEDS];
  memcpy(new_house_pos, house_pos, LED_Control::NUM_LEDS);

  const String& new_ids = request->arg("house_ids");
  for (unsigned int offset = 0; offset < new_ids.length() - 3; offset += 4) {
    uint8_t ziel = parseAsInt<uint8_t>(new_ids, offset, 2) - 1;
    uint8_t ziel2 = parseAsInt<uint8_t>(new_ids, offset + 2, 2) - 1;
    Serial.printf("Setze:\n%u -> %u\n%u -> %u\n", new_house_pos[ziel2], ziel, new_house_pos[ziel], ziel2);
    uint8_t buff = new_house_pos[ziel];
    new_house_pos[ziel] = new_house_pos[ziel2];
    new_house_pos[ziel2] = buff;
    Serial.printf("Next Char: %c (%u)\n", new_ids[offset + 4], new_ids[offset + 4]);
  }
  storage->House_Positions->Save(new_house_pos);

  request->redirect("/");
}

void WIFI::Handle_ResetHousePos(AsyncWebServerRequest* request) {
  storage->House_Positions->Reset();

  request->redirect("/");
}

void WIFI::Handle_LoadConfigurationVariable(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(1);
  Serial.print("Lade Konfigurationsvariable ");
  const String& a = request->arg(0U);
  uint8_t index = parseAsInt<uint8_t>(a);

  const Storage::Object* obj = storage->GetObject(index);
  if (obj == nullptr) {
    request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
    return;
  }
  Serial.printf("'%s' (%u): ", obj->GetName(), index);
  const char* value = *obj;
  if (obj->GetType() == Storage::Object::Type::Text) {
    Serial.printf("'%s'\n", value);
    request->send(200, "text/html", value);
  } else {
    const uint16_t length = obj->GetLength();
    char output[length * 5 + 1];
    char* output_ptr = output;

    for (uint16_t i = 0; i < length; i++) {
      sprintf(output_ptr, "0x%02x ", value[i]);
      output_ptr += strlen(output_ptr);
    }
    *output_ptr = '\0';

    Serial.printf("'%s'\n", output);
    request->send(200, "text/html", output);
  }
}

void WIFI::Handle_SaveConfigurationVariable(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(2);
  const String& name = request->arg(0U);
  const String& value = request->arg(1U);
  uint16_t length = value.length();

  const uint8_t index = parseAsInt<uint8_t>(name);

  Storage::Object* obj = storage->GetObject(index);
  if (obj == nullptr) {
    request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
    return;
  }

  Serial.printf("Save to configuration '%s' (%u) the value(s): '", obj->GetName(), index);
  Serial.print(value);
  Serial.println("'");

  if (obj->GetType() == Storage::Object::Type::Text) {
    length++;
  } else {
    length /= 5;
  }
  uint8_t data[length] = { 0 };

  if (obj->GetType() != Storage::Object::Type::Text) {
    Serial.print("Bytes: ");

    for (uint16_t i = 0; i < length; i++) {
      if (value[i * 5 + 2] >= '0' && value[i * 5 + 2] <= '9')
        data[i] = (value[i * 5 + 2] - '0') * 16;
      else
        data[i] = (value[i * 5 + 2] - 'a' + 10) * 16;

      if (value[i * 5 + 3] >= '0' && value[i * 5 + 3] <= '9')
        data[i] += value[i * 5 + 3] - '0';
      else
        data[i] += value[i * 5 + 3] - 'a' + 10;

      Serial.printf("%d ", value[i]);
    }
    Serial.println();
  } else {
    value.getBytes(data, length);
  }

  if (obj->Save(data, length)) {
    request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
  } else {
    request->send(200, "text/html", "Konfiguration gespeichert!");
  }
}

void WIFI::Handle_Reset(AsyncWebServerRequest* request) {
  if (request->args() > 0) {
    request->send(200, "text/html", "ESP-Einstellung wird zurückgesetzt");
    if (request->argName(0) == "id")
      storage->Reset(parseAsInt<uint8_t>(request->arg(0U)));
    else if (request->argName(0) == "wifi")
      storage->ResetAll(request->args() == 1 && request->arg(0U) == "true");
  } else {
    request->send(200, "text/html", "ESP wird zurückgesetzt");
    storage->ResetAll(false);
  }

  switchingtimes->Load();
  LED_control->LoadColors();
}


// ============================== Switches ==============================

void WIFI::Handle_LoadSwitchConfiguration(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(1);
  const String& index_str = request->arg(0U);
  uint8_t index = parseAsInt<uint8_t>(index_str);

  const char* name;
  uint8_t type;
  uint8_t pin;
  uint8_t pin2;
  const uint8_t* fun;

  if (switches->GetConfiguration(index, &name, &type, &pin, &pin2, &fun)) {
    char wp[60];
    sprintf(wp, "0 %s;%d;%d;%d;", name, type, pin, pin2);
    char* wp2 = wp + strlen(wp);
    for (uint8_t i = 0; i < 6; i++, wp2 += strlen(wp2))
      sprintf(wp2, "%d;", fun[i]);

    request->send(200, "text/html", wp);
  } else { // Index wrong
    request->send(200, "text/html", "Wrong index!");
  }
}

void WIFI::Handle_SaveSwitch(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(11);
  uint8_t id = parseAsInt<uint8_t>(request->arg(0U));
  const String& name = request->arg(1U);
  char name_cc[20] = { '\0' };
  strncpy(name_cc, name.c_str(), name.length());
  uint8_t type = parseAsInt<uint8_t>(request->arg(2U));
  uint8_t pin = parseAsInt<uint8_t>(request->arg(3U));
  uint8_t pin2 = parseAsInt<uint8_t>(request->arg(4U));
  uint8_t fun[Switches_Storage::AM_SWITCH_FUNS];
  for (uint8_t i = 0; i < Switches_Storage::AM_SWITCH_FUNS; i++)
    fun[i] = parseAsInt<uint8_t>(request->arg(i + 5));

  if (switches->SetConfiguration(id, name_cc, type, pin, pin2, fun))
    request->send(200, "text/html", "0");
  else {
    Serial.printf("Handle_SaveSwitch: ID falsch: %u\n", id);
    request->send(200, "text/html", "Schalter ID falsch!");
  }
}


// ============================== Windmill ==============================

void WIFI::Handle_SaveWindmillConfiguration(AsyncWebServerRequest* request) {
  CHECK_ARGUMENT_COUNT(16);
  uint8_t outputpin = parseAsInt<uint8_t>(request->arg(0U));
  uint8_t inputpin = parseAsInt<uint8_t>(request->arg(1U));
  uint16_t action_duration = parseAsInt<uint16_t>(request->arg(2U));
  bool weather_enabled = request->arg(3U)[0] == '1';
  const String& app_id = request->arg(4U);
  const String& city_id = request->arg(5U);
  uint8_t wind_trigger = parseAsInt<uint8_t>(request->arg(6U));
  uint8_t mode = parseAsInt<uint8_t>(request->arg(7U));
  uint8_t speed_mode = parseAsInt<uint8_t>(request->arg(8U));
  uint8_t constant_speed = parseAsInt<uint8_t>(request->arg(9U));
  uint8_t fun_min = parseAsInt<uint8_t>(request->arg(10U));
  uint8_t fun_max = parseAsInt<uint8_t>(request->arg(11U));
  uint16_t fun_freq = parseAsInt<uint16_t>(request->arg(12U));
  uint8_t live_min = parseAsInt<uint8_t>(request->arg(13U));
  uint8_t live_max = parseAsInt<uint8_t>(request->arg(14U));
  float live_turn_off = StringToFloat(request->arg(15U));

  if (windmill->SetConfiguration(outputpin, inputpin, action_duration, weather_enabled, app_id.c_str(), city_id.c_str(), wind_trigger, mode, speed_mode, constant_speed, fun_min, fun_max, fun_freq, live_min, live_max, live_turn_off))
    request->send(200, "text/html", "0");
  else
    request->send(200, "text/html", "Konfiguration gespeichert! Neustart notwendig!");
  Serial.printf("Handle_SaveSwitch: Too less arguments: %u/7 (URL: '%s')\n", request->args(), request->url().c_str());
}
