/**
 * @file Switch.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-06
 *
 * Remarks:
 */

#include "Switch.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <Arduino.h>
#include "Classes.h"
#include "Storage.h"

 // Handler for swichting lights during test
TaskHandle_t Switches_Handler;

const uint32_t CLICK_TIME_SHORT = 100; // Minimum, in ms

const uint32_t CLICK_TIME_LONG = 500; // Minimum, in ms

const uint32_t DEBOUNCE_TIME = 50; // in ms

void(**switch_functions)();

// Switches

Switches::Switches(void(*_funs[])()) {
  switch_functions = _funs;

  Load();

  xTaskCreatePinnedToCore(Runner_static, "Switches", 5000, this, 1, &Switches_Handler, 1);
}

void Switches::Load() {
  const void* switches_ptr = *(storage->Switches);
  Switches_Storage* switch_configurations = (Switches_Storage*)switches_ptr;
  for (uint16_t i = 0; i < AM_Switches; i++)
    switches[i] = Switch(switch_configurations[i]);
}

const std::array<const char*, Switches::AM_Switches> Switches::GetNames() const {
  std::array<const char*, Switches::AM_Switches> names;
  for (uint8_t i = 0; i < AM_Switches; i++) {
    names[i] = switches[i].GetName();
  }
  return names;
}

bool Switches::GetConfiguration(uint8_t _index, const char** _name, uint8_t* _type, uint8_t* _pin, uint8_t* _pin2, const uint8_t** _fun) const {
  if (_index >= AM_Switches)
    return false;

  switches[_index].GetConfiguration(_name, _type, _pin, _pin2, _fun);

  return true;
}

bool Switches::SetConfiguration(uint8_t _id, const char* _name, uint8_t _type, uint8_t _pin, uint8_t _pin2, uint8_t* _fun) {
  if (_id > AM_Switches)
    return false;

  switches[_id].SetConfiguration(_id, _name, _type, _pin, _pin2, _fun);
  return true;
}

void Switches::Reset() {
  for (uint16_t i = 0; i < AM_Switches; i++) {
    char val[Storage::LENGTH_PER_SWITCH] = { 0 };
    sprintf(val + 9, "Schalter %u", i + 1);
    storage->Switches->Save(val, Storage::LENGTH_PER_SWITCH, Storage::LENGTH_PER_SWITCH * i);
  }

  Load();
}

void Switches::Runner_static(void* _this) {
  ((Switches*)_this)->Runner();
}

void Switches::Runner() {
  while (1) {
    for (uint8_t i = 0; i < AM_Switches; i++)
      switches[i].Check_Status();
    vTaskDelay(50 / portTICK_PERIOD_MS);
  }
}


// Switch
/**
 * @brief Construct a new Switches:: Switch:: Switch object
*/
Switches::Switch::Switch() {

}

Switches::Switch::Switch(Switches_Storage& _ss): Switches_Storage(_ss) {
  pinMode(pin, INPUT_PULLUP);
  if (type == Type::Switch3)
    pinMode(pin2, INPUT_PULLUP);
  Serial.printf(" Switch %s:\n  Type: %d\n  Pin: %d\n  Pin 2: %d\n  Fun: %d, %d, %d, %d, %d, %d\n", name, type, pin, pin2, fun[0], fun[1], fun[2], fun[3], fun[4], fun[5]);
}

/**
 * @brief Checks the current button status and calls associated function
 */
void Switches::Switch::Check_Status() {
  switch (this->type) {
  case Type::None:
    break;
  case Type::Button:
    Check_Button();
    break;
  case Type::Switch2:
    Check_Switch2();
    break;
  case Type::Switch3:
    Check_Switch3();
    break;
  default:
    //Serial.printf("Undefined switch type!\n");
    break;
  }
}

/**
 * @brief Get the name of the switch
 *
 * @retval const char*
 */
const char* Switches::Switch::GetName() const {
  return name;
}

void Switches::Switch::GetConfiguration(const char** _name, uint8_t* _type, uint8_t* _pin, uint8_t* _pin2, const uint8_t** _fun) const {
  *_name = name;
  *_type = type;
  *_pin = pin;
  *_pin2 = pin2;
  *_fun = fun;
}

bool Switches::Switch::SetConfiguration(uint8_t _id, const char* _name, uint8_t _type, uint8_t _pin, uint8_t _pin2, uint8_t* _fun) {
  if (type == Type::None) {
    pinMode(pin, INPUT);
    pinMode(pin2, INPUT);
  } else {
    pinMode(_pin, INPUT_PULLUP);
    if (type == Type::Switch3)
      pinMode(_pin2, INPUT_PULLUP);
  }

  strcpy(name, _name);
  type = (Type)_type;
  pin = _pin;
  pin2 = _pin2;
  memcpy(fun, _fun, AM_SWITCH_FUNS);

  storage->Switches->Save((Switches_Storage*)this, Storage::LENGTH_PER_SWITCH, _id * Storage::LENGTH_PER_SWITCH);

  Serial.printf("Changed switch: %d - %s\n Type: %d\n Pin: %d\n Pin 2: %d\n Fun: %d, %d, %d, %d, %d, %d\n", _id, _name, _type, _pin, _pin2, _fun[0], _fun[1], _fun[2], _fun[3], _fun[4], _fun[5]);

  return true;
}

void Switches::Switch::run_fun(uint8_t _index) {
  if (fun[_index] == 0)
    return;
  else if (fun[_index] - 1 < Switches::AM_AVAILABLE_SWITCH_FUNS) {
    switch_functions[fun[_index] - 1]();
  }
}

void Switches::Switch::Check_Button() {
  if (LED_control->Getlamptesttype() == Lampteststatus::CycleSwitch) {
    if (digitalRead(this->pin)) // Button lifted
      this->event_type = Event_Type::Lifted;
    else {
      if (this->event_type == Event_Type::Lifted)
        LED_control->SwitchLampTest(Lampteststatus::CycleSwitch);

      if (LED_control->Getlamptesttype() == Lampteststatus::CycleSwitch)
        this->event_type = Event_Type::Pressed;
      else
        this->event_type = Event_Type::Lifted;
    }
  } else {
    if (digitalRead(this->pin)) { // Button lifted
      if (this->event_type == Event_Type::Lifted) { // Button still lifted
        run_fun(Functions::Button::lifted);
      } else { // Button newly lifting
        run_fun(Functions::Button::lifting);

        uint32_t click_time = millis() - this->down_time;
        if (click_time > CLICK_TIME_SHORT) { // Click
          if (click_time < CLICK_TIME_LONG) { // Short click
            run_fun(Functions::Button::clicked_short);
          } else { // Long click
            run_fun(Functions::Button::clicked_long);
          }
        }
        this->event_type = Event_Type::Lifted;
      }
    } else { // Button pressed
      if (this->event_type == Event_Type::Pressed) { // Button still pressed
        run_fun(Functions::Button::pressed);
      } else { // Button newly pressing
        run_fun(Functions::Button::pressing);
        this->event_type = Event_Type::Pressed;

        if (millis() - this->down_time > DEBOUNCE_TIME)
          this->down_time = millis();
      }
    }
  }
}

void Switches::Switch::Check_Switch2() {
  if (!digitalRead(this->pin)) { // Pos 1
    if (this->event_type != Event_Type::Pos1) { // Shifting
      run_fun(Functions::Switch2::position1);
      this->event_type = Event_Type::Pos1;
    }
  } else { // Pos 0
    if (this->event_type != Event_Type::Pos0) { // Shifting
      run_fun(Functions::Switch2::position0);
      this->event_type = Event_Type::Pos0;
    }
  }
}

void Switches::Switch::Check_Switch3() {
  if (!digitalRead(this->pin)) { // Pos 1
    if (this->event_type != Event_Type::Pos1) { // Shifting
      run_fun(Functions::Switch3::position1);
      this->event_type = Event_Type::Pos1;
    }
  } else if (!digitalRead(this->pin2)) { // Pos 2
    if (this->event_type != Event_Type::Pos2) { // Shifting
      run_fun(Functions::Switch3::position2);
      this->event_type = Event_Type::Pos2;
    }
  } else { // Pos 0
    if (this->event_type != Event_Type::Pos0) { // Shifting
      run_fun(Functions::Switch3::position0);
      this->event_type = Event_Type::Pos0;
    }
  }
}


// Switches_Storage
/**
 * @brief Construct a new Switches_Storage::Switches_Storage object
*/
Switches_Storage::Switches_Storage() {

}