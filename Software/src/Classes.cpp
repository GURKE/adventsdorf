/**
 * @file Classes.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-12
 *
 * Remarks:
*/
#include "Classes.h"
#include "SPIFFS.h" // https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/

void listDir(fs::FS& fs, const char* dirname, uint8_t levels);

void fun_Set_Mode_Off() { LED_control->SetMode(LED_Control::Mode::Off); Serial.println("Klick: Modus, Aus"); }
void fun_Set_Mode_Auto() { LED_control->SetMode(LED_Control::Mode::Auto); Serial.println("Klick: Modus, Auto"); }
void fun_Set_Mode_On() { LED_control->SetMode(LED_Control::Mode::On); Serial.println("Klick: Modus, An"); }
void fun_Light_Test_Stop() { LED_control->SwitchLampTest(Lampteststatus::Off); Serial.println("Klick: Test, Aus"); }
void fun_Light_Test_Complete() { LED_control->SwitchLampTest(Lampteststatus::Complete); Serial.println("Klick: Test, Komplett"); }
void fun_Light_Test_Cylce_Time() { LED_control->SwitchLampTest(Lampteststatus::CycleTime); Serial.println("Klick: Test, Zyklus Zeit"); }
void fun_Light_Test_Cylce_Click() { LED_control->SwitchLampTest(Lampteststatus::CycleSwitch); Serial.println("Klick: Test, Zyklus Klick"); }
void fun_Windmill_Off() { windmill->change_mode_off(); Serial.println("Klick: Windmühle aus"); }
void fun_Windmill_Motion() { windmill->change_mode_motion(); Serial.println("Klick: Windmühle automatisch"); }
void fun_Windmill_MotionSwitchtime() { windmill->change_mode_motionswitchtime(); Serial.println("Klick: Windmühle automatisch"); }
void fun_Windmill_On() { windmill->change_mode_on(); Serial.println("Klick: Windmühle ein"); }

void(*functions[Switches::AM_AVAILABLE_SWITCH_FUNS])() = { fun_Set_Mode_Off, fun_Set_Mode_Auto, fun_Set_Mode_On, fun_Light_Test_Stop, fun_Light_Test_Complete, fun_Light_Test_Cylce_Time, fun_Light_Test_Cylce_Click, fun_Windmill_Off, fun_Windmill_Motion, fun_Windmill_MotionSwitchtime, fun_Windmill_On };

WIFI* wifi;
Storage* storage;
SwitchingTimes* switchingtimes;
LED_Control* LED_control;
Switches* switches;
Windmill* windmill;

struct tm main_time;

bool CheckTime() {
  return switchingtimes->CheckTime();
}

void Init() {
  storage = new Storage();

  Serial.println("Initializing SPIFFS:");
  if (!SPIFFS.begin(true)) {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  listDir(SPIFFS, "/", 0);
  Serial.println("SPIFFS initalized\n");

  Serial.println("Initializing LED control:");
  switchingtimes = new SwitchingTimes();
  taskYIELD();
  Serial.println("Switching times initalized\n");

  Serial.println("Initializing LED control:");
  LED_control = new LED_Control();
  taskYIELD();
  Serial.println("LED control initalized\n");

  Serial.println("Initializing WIFI:");
  wifi = new WIFI();
  taskYIELD();
  Serial.println("WiFi initalized\n");

  Serial.println("Initializing Windmill:");
  windmill = new Windmill();
  taskYIELD();
  Serial.println("Windmill initalized\n");

  Serial.println("Initializing Switches:");
  switches = new Switches(functions);
  taskYIELD();
  Serial.println("Switches initalized\n");
}

void listDir(fs::FS& fs, const char* dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\r\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("- failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println(" - not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("\tSIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}
