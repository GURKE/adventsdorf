/**
 * @file Windmill.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-14
 *
 * Remarks:
*/

#include "Windmill.h"
#include "Classes.h"
#include "Arduino.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "StaticFunctions.h"

// Handler for swichting lights during test
TaskHandle_t Windmill_Handler;

/**
 * @brief Construct a new Windmill
*/
Windmill::Windmill() : auto_on(false), weather_app_id(url + 64), weather_city_id(url + 50) {
  const void* windmill_ptr = *(storage->Windmill);
  *((Windmill_Storage*)this) = *((Windmill_Storage*)windmill_ptr);

  for (uint8_t i = 0; i < 16; i++) {
    sprintf(weather_app_id + i * 2, "%x", weather_app_id_raw[i] >> 4);
    sprintf(weather_app_id + i * 2 + 1, "%x", weather_app_id_raw[i] % 16);
  }
  weather_app_id[32] = '&';

  sprintf(weather_city_id, "%07u", weather_city_id_raw);
  weather_city_id[7] = '&';

  Serial.printf("Saved Windmill configurations:\n Inputpin: %u\n Outputpin: %u\n Action_Duration: %u s\n Weather enabled: %s\n Weather App ID: %.32s\n Weather City ID: %.7s\n Wind_Speed_Trigger: %u\n URL: '%s'\n", inputpin, outputpin, action_duration, weather_enabled ? "Enabled" : "Disabled", weather_app_id, weather_city_id, weather_wind_speed_trigger, url);

  pinMode(inputpin, INPUT);
  pinMode(outputpin, OUTPUT);

  xTaskCreatePinnedToCore(Runner_static, "Windmill", 5000, this, 1, &Windmill_Handler, 1);
}

/**
 * @brief Caller function for Runner() to get called by xTask
 *
 * @param _this The windmill-object
*/
void Windmill::Runner_static(void* _this) {
  ((Windmill*)_this)->Runner();
}

bool Windmill::running() const {
  return mode == Mode::On || auto_on;
}

/**
 * @brief Checks motion detection and de-/activates windmill
*/
void Windmill::Runner() {
  while (1) {
    switch (mode) {
    case Mode::On:
      analogWrite(outputpin, CalculateSpeed());
      break;
    case Mode::Off:
      analogWrite(outputpin, 0);
      break;
    case Mode::Motion: case Mode::MotionSwitchtime:
      if (mode == Mode::MotionSwitchtime && !switchingtimes->CheckTime()) { // Only run windmill during switching times
        analogWrite(outputpin, 0);
      } else {
        if (weather_enabled)
          GetRealWindSpeed();

        if (digitalRead(inputpin)) { // Motion detected
          if (!weather_enabled || weather_wind_speed > weather_wind_speed_trigger) {
            if (!auto_on)
              Serial.printf("%lu: Motion detected! Windmill activated!\n", millis());
            auto_on = true;
            analogWrite(outputpin, CalculateSpeed());
            last_motion_triggered_time = millis();
          } else {
            if (last_motion_triggered_time + 5000 < millis())
              Serial.printf("%lu: Motion detected! Wind speed (%u m/s) too low!\n", millis(), weather_wind_speed);
            last_motion_triggered_time = millis();
          }
        } else {
          if (auto_on) {
            if (last_motion_triggered_time + 1000 * (action_duration - 2) < millis()) { // The motion sensor sends a 2 seconds signal
              Serial.printf("%lu: Switch windmill deactivated!\n", millis());
              auto_on = false;
              analogWrite(outputpin, 0);
            }
          }
        }
      }
      break;
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

/**
 * @brief Get windspeed from internet
 * @return true, if loading was successfull
 */
bool Windmill::GetRealWindSpeed() {
  if (next_weather_check < millis() && WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.begin(url);
    int httpCode = http.GET();
    if (httpCode > 0) {
      String payload = http.getString();
      Serial.print("Response: '");
      Serial.print(payload);
      Serial.println("'");
      DynamicJsonDocument doc(1024);
      deserializeJson(doc, payload);
      weather_wind_speed = ((float)doc["wind"]["speed"]);
      next_weather_check = millis() + WEATHER_CHECK_WAITING_TIME;
      Serial.printf("Loaded windspeed: %u m/s\n", weather_wind_speed);
      http.end();
      return true;
    } else {
      Serial.println("Wetterdaten konnten nicht geladen werden!");
      http.end();
      return false;
    }
  }
  return false;
}

/**
 * @brief Calculate the new windmill speed
 * @return uint8_t Windmillspeed [0, 255]
 */
uint8_t Windmill::CalculateSpeed() {
  if (speed_mode == Speedmode::Const)
    return sm_constant_speed;
  else if (speed_mode == Speedmode::Function)
    return sm_function_min + (sm_function_max - sm_function_min) * (1 + sin((float)(millis() * sm_function_frequency * 2 * PI) / 3600 / 1000)) / 2;
  else { // Livewind
    GetRealWindSpeed();
    if (weather_wind_speed > sm_live_turnoff)
      return 0;
    else
      return (sm_live_max - sm_live_min) * weather_wind_speed / sm_live_turnoff + sm_live_min;
  }
}

/**
 * @brief Returns if the live weather feature is active
 *
 * @retval True, if live weather is used
*/
bool Windmill::GetLiveWeatherActive() const {
  return weather_enabled;
}

/**
 * @brief Return the trigger windspeed in m/s
 *
 * @retval uint8_t windspeed in m/s
*/
uint8_t Windmill::GetWindSpeedTrigger() const {
  return weather_wind_speed_trigger;
}

/**
 * @brief Get the city id defined by openweathermap.org
 *
 * @retval const char* city id
*/
const char* Windmill::GetCityID() const {
  return weather_city_id;
}

/**
 * @brief Get the appid defined by openweathermap.org
 *
 * @retval const char* app id
*/
const char* Windmill::GetAppID() const {
  return weather_app_id;
}

/**
 * @brief Get the pin of the windmill
 *
 * @retval uint8_t Pin of windmill
*/
uint8_t Windmill::GetPinWindmill() const {
  return outputpin;
}

/**
 * @brief Get the pin of the motion detector
 *
 * @retval uint8_t Pin of motion detector
*/
uint8_t Windmill::GetPinMotiondetector() const {
  return inputpin;
}

/**
 * @brief Get duration of a windmill action in seconds
 *
 * @retval uint16_t Duration of a windmill action in seconds
*/
uint16_t Windmill::GetActionduration() const {
  return action_duration;
}

/**
 * @brief Set the Configuration object
 *
 * @param _outputpin Outputpin for the windmill
 * @param _inputpin Inputput for the motion detector
 * @param _action_duration Action duration in seconds after triggering
 * @param _weather_enabled Enable triggering by live wind
 * @param _city_id City id of openweathermap.org for live wind
 * @param _app_id City id of openweathermap.org for live wind
 * @param _weather_wind_speed_trigger Least wind speed the windmill starts to run
 * @param _mode Mode (Off, Auto, On)
 * @param _speed_mode Speedmode (Const, Function, Live)
 * @param _constant_speed Rotation speed [1, 255]
 * @param _fun_min Function mode minimum speed [1, 255]
 * @param _fun_max Function mode maximum speed [1, 255]
 * @param _fun_freq Function mode frequency [1, 65535]
 * @param _live_min Live mode minimum speed [1, 255]
 * @param _live_max Live mode maximum speed [1, 255]
 * @param _live_turn_off Live mode Turn off speed
 * @retval Returns true if configuration could saved correctly
*/
bool Windmill::SetConfiguration(uint8_t _outputpin, uint8_t _inputpin, uint16_t _action_duration, bool _weather_enabled, const char* _app_id, const char* _city_id, uint8_t _weather_wind_speed_trigger, uint8_t _mode, uint8_t _speed_mode, uint8_t _constant_speed, uint8_t _fun_min, uint8_t _fun_max, uint16_t _fun_freq, uint8_t _live_min, uint8_t _live_max, float _live_turn_off) {
  bool ret = true;
  if (outputpin != _outputpin) {
    pinMode(outputpin, INPUT);
    outputpin = _outputpin;
    pinMode(outputpin, OUTPUT);
    Serial.println("Outputpin changed, need restart!");
    ret = false;
  }
  inputpin = _inputpin;
  pinMode(inputpin, INPUT);
  action_duration = _action_duration;
  weather_enabled = _weather_enabled;
  memcpy(weather_app_id, _app_id, 32);
  memcpy(weather_city_id, _city_id, 7);
  weather_city_id[7] = '&';
  weather_wind_speed_trigger = _weather_wind_speed_trigger;
  mode = (Mode)_mode;
  speed_mode = (Speedmode)_speed_mode;
  sm_constant_speed = _constant_speed;
  sm_function_min = _fun_min;
  sm_function_max = _fun_max;
  sm_function_frequency = _fun_freq;
  sm_live_min = _live_min;
  sm_live_max = _live_max;
  sm_live_turnoff = _live_turn_off;

  parseAsHex(weather_app_id, weather_app_id_raw);

  sscanf(_city_id, "%u", &weather_city_id_raw);

  storage->Windmill->Save((Windmill_Storage*)this);

  Serial.printf("Saved Windmill configurations:\n Inputpin: %u\n Outputpin: %u\n Action_Duration: %u s\n Weather: %s\n Weather App ID: %.32s\n Weather City ID: %.7s\n Wind_Speed_Trigger: %u\n URL: '%s'\n", inputpin, outputpin, action_duration, weather_enabled ? "Enabled" : "Disabled", weather_app_id, weather_city_id, weather_wind_speed_trigger, url);

  return ret;
}

/**
 * @brief Switches windmill on
*/
void Windmill::change_mode_on() {
  mode = Mode::On;
}

/**
 * @brief Switches windmill off
*/
void Windmill::change_mode_off() {
  mode = Mode::Off;
}

/**
 * @brief Switches windmill on, when lights are on and motion
*/
void Windmill::change_mode_motionswitchtime() {
  mode = Mode::MotionSwitchtime;
}

/**
 * @brief Switches windmill on, when there is motion
*/
void Windmill::change_mode_motion() {
  mode = Mode::Motion;
}

/**
 * @brief Get the Mode (Off, MotionSwitchtime, Motion, On)
 * @retval Mode
*/
Windmill::Mode Windmill::getMode() const {
  return mode;
}

/**
 * @brief Get the Speed Mode (Const, Function, Live)
 * @return Speed Mode
 */
Windmill::Speedmode Windmill::getSpeedMode() const {
  return speed_mode;
}

/**
 * @brief return Rotation speed (if speed_mode == Speedmode::Const) [1, 255]
 */
uint8_t Windmill::Get_Constant_Speed() const {
  return sm_constant_speed;
}

/**
 * @brief return Minimum rotation speed (if speed_mode == Speedmode::Function) [1, 255]
 */
uint8_t Windmill::Get_Function_Min() const {
  return sm_function_min;
}

/**
 * @brief return Maximum rotation speed (if speed_mode == Speedmode::Function) [1, 255]
 */
uint8_t Windmill::Get_Function_Max() const {
  return sm_function_max;
}

/**
 * @brief return Frequency rotation speed (if speed_mode == Speedmode::Function) [1, 255]
 */
uint16_t Windmill::Get_Function_Frequency() const {
  return sm_function_frequency;
}

/**
 * @brief return Minimum rotation speed (if speed_mode == Speedmode::Live) [1, 255]
 */
uint8_t Windmill::Get_Live_Min() const {
  return sm_live_min;
}

/**
 * @brief return Maximum rotation speed (if speed_mode == Speedmode::Live) [1, 255]
*/
uint8_t Windmill::Get_Live_Max() const {
  return sm_live_max;
}

/**
 * @brief return Turn off real speed (if speed_mode == Speedmode::Live) [1, 255]
*/
float Windmill::Get_Live_Turnoff() const {
  return sm_live_turnoff;
}