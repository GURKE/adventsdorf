
function openTab(button, pageid) {
  if (button != null) {
    const tabcontent = document.getElementsByClassName("tabcontent");
    for (let tabPage of tabcontent) {
      if (tabPage.id == pageid) {
        tabPage.style.display = "block";
      } else {
        tabPage.style.display = "none";
      }
    }
    const tablinks = document.getElementsByClassName("tablinks");
    for (let tabButton of tablinks) {
      if (tabButton == button) {
        tabButton.classList.add("active");
      } else {
        tabButton.classList.remove("active");
      }
    }
  }
}


/* Mainpage */

function Start_Loading() {
  document.getElementById("latex_loading_animation").classList.add("loading_animation");
}

function End_Loading() {
  document.getElementById("latex_loading_animation").classList.remove("loading_animation");
}


// Housing-Lamp-Status Updates
setInterval(function () {
  getData();
}, 1000);

var testtype = 0;
function getData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var i;
      for (i = 1; i < 25; i++) {
        const houseDiv = document.getElementById('Haus' + (i < 10 ? '0' : '') + i);
        if (this.responseText[i - 1] == '0') {
          houseDiv.classList.remove("house_on");
        } else {
          houseDiv.classList.add("house_on");
        }
      }
      if (this.responseText[i - 1] == '0')
        activate_testbuttons();
      else
        deactivate_testbuttons();

      testtype = this.responseText[i - 1] - '0';
    }
  };
  xhttp.open('GET', 'lampstatus', true);
  xhttp.send();
}

var colors = Array();
function createHouses() {
  var sheet = document.styleSheets[0];

  for (var i = 0; i < 24; i++) {
    var argument = 'id=' + i;

    var xhttp = new XMLHttpRequest();
    xhttp.house_id = i;
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        if (this.responseText[0] == '0') {// Success
          colors[this.house_id] = '#' + this.responseText.substring(2);
          var housename = "";
          if (this.house_id + 1 < 10)
            housename += "0";
          housename += (this.house_id + 1).toString();

          document.getElementById('Haus' + housename).style.setProperty('--house-clr', colors[this.house_id]);
        }
      }
    };
    xhttp.open('GET', 'get_house_color?' + argument, true);
    xhttp.send();
  }
}
createHouses();

// Tests
function deactivate_testbuttons() {
  document.getElementById('button_test_single').disabled = true;
  document.getElementById('button_test_complete').disabled = true;
  document.getElementById('button_test_cycleswitch').disabled = true;
  document.getElementById('button_test_cycletime').disabled = true;
  document.getElementById('button_test_stop').disabled = false;
}

function activate_testbuttons() {
  document.getElementById('button_test_single').disabled = false;
  document.getElementById('button_test_complete').disabled = false;
  document.getElementById('button_test_cycleswitch').disabled = false;
  document.getElementById('button_test_cycletime').disabled = false;
  document.getElementById('button_test_stop').disabled = true;
  document.getElementById('button_test_single').setAttribute('value', 'Test starten');
  document.getElementById('button_test_complete').setAttribute('value', 'Test starten');
  document.getElementById('button_test_cycleswitch').setAttribute('value', 'Test starten');
  document.getElementById('button_test_cycletime').setAttribute('value', 'Test starten');
}

function test_single_activate(event) {
  if (testtype == 0) {
    deactivate_testbuttons();
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'SingleLampTest', true);
    xhttp.send();
    document.getElementById('button_test_single').setAttribute('value', 'Test aktiv');
    testtype = 1;
  }
  return false;
}

function test_complete_activate(event) {
  if (testtype == 0) {
    deactivate_testbuttons();
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'CompleteLampTest', true);
    xhttp.send();
    document.getElementById('button_test_complete').setAttribute('value', 'Test aktiv');
    testtype = 2;
  }
  return false;
}

function test_cycleswitch_activate(event) {
  if (testtype == 0) {
    deactivate_testbuttons();
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'CycleLampTestSwitch', true);
    xhttp.send();
    document.getElementById('button_test_cycleswitch').setAttribute('value', 'Test aktiv');
    testtype = 3;
  }
  return false;
}

function test_cycletime_activate(event) {
  if (testtype == 0) {
    deactivate_testbuttons();
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'CycleLampTestTime', true);
    xhttp.send();
    document.getElementById('button_test_cycletime').setAttribute('value', 'Test aktiv');
    testtype = 4;
  }
  return false;
}

function test_stop(event) {
  var xhttp = new XMLHttpRequest();
  xhttp.open('GET', 'TestStop', true);
  xhttp.send();

  activate_testbuttons();

  testtype = 0;
  return false;
}

function clickhouse(ev) {
  if (testtype == 1) {
    const number = parseInt(ev.id.substring("Haus".length))
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'SingleLampTest?i=' + number, true);
    xhttp.send();
  }
}


/* Switching times page */

function switching_times_change() {
  Start_Loading();
  var cv = document.getElementById("select_times");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        //this.responseText: 0 HH:mm-HH:mm w
        document.getElementById('switching_time_enabled').checked = this.responseText[18] != '0';
        document.getElementById('switching_time_activate_time').value = this.responseText.substring(2, 7);
        document.getElementById('switching_time_deactivate_time').value = this.responseText.substring(8, 13);
        var all_weekdays_on = true;
        var weekdays = (this.responseText[14] - '0') * 100 + (this.responseText[15] - '0') * 10 + (this.responseText[16] - '0');
        for (var i = 0; i < 7; i++) {
          document.getElementById('switching_time_weekdays_' + i).checked = weekdays & (1 << i);
          if (!(weekdays & (1 << i)))
            all_weekdays_on = false;
        }
        document.getElementById('switching_time_weekdays_a').checked = all_weekdays_on;
      } else {
        alert("Laden der Schaltzeit fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'load_switch_time?id=' + cv.value, true);
  xhttp.send();
}

function switching_time_weekdays_change(event) {
  if (event.target.id == 'switching_time_weekdays_a') {// All-selector
    for (var i = 0; i < 7; i++)
      document.getElementById('switching_time_weekdays_' + i).checked = event.target.checked;
  } else {
    if (event.target.checked) {
      for (var i = 0; i < 7; i++)
        if (!document.getElementById('switching_time_weekdays_' + i).checked)
          return;
      document.getElementById('switching_time_weekdays_a').checked = true;
    }
    else
      document.getElementById('switching_time_weekdays_a').checked = false;
  }
}

function save_switching_time(event) {
  Start_Loading();
  var switching_time_text = 'id=' + document.getElementById('select_times').value.toString();
  switching_time_text += '&activate=' + document.getElementById('switching_time_activate_time').value.toString();
  switching_time_text += '&deactivate=' + document.getElementById('switching_time_deactivate_time').value.toString();
  var weekdays = 0;
  for (var i = 0; i < 7; i++)
    if (document.getElementById('switching_time_weekdays_' + i).checked)
      weekdays += 1 << i;
  switching_time_text += '&weekdays=' + weekdays;
  switching_time_text += '&enabled=' + (document.getElementById('switching_time_enabled').checked ? '1' : '0');

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        alert("Gespeichert");
      } else {
        alert("Speichern der Schaltzeit fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'save_switch_time?' + switching_time_text, true);
  xhttp.send();

  return false;
}

/* Colors */
function colors_color_change(event, single) {
  Start_Loading();
  var argument = 'id=';
  if (single) {
    argument += document.getElementById('colors_single_number').value - 1;
    argument += '&color=' + document.getElementById('colors_single_color').value.substring(1);
  } else {
    argument += 'a';
    argument += '&color=' + document.getElementById('colors_common_color').value.substring(1);
  }

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      End_Loading();
      if (this.responseText[0] == '0') { } else {
        alert("Farbe konnte nicht aktiviert werden! " + this.responseText);
      }
    }
  };
  xhttp.open('GET', 'try_color?' + argument, true);
  xhttp.send();
}

/* Common */
function color_common_variance_changed(event) {
  var value = document.getElementById('colors_common_variance_value').value;
  document.getElementById('colors_common_variance_label').innerHTML = ' (' + value + ' %)';
}

function color_common_save(event) {
  Start_Loading();
  var argument = 'color=' + document.getElementById('colors_common_color').value.substring(1);
  argument += '&variance=' + document.getElementById('colors_common_variance_value').value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {
        for (var i = 0; i < 24; i++) {
          var housename = "";
          if (i + 1 < 10)
            housename += "0";
          housename += (i + 1).toString();

          document.getElementById("Haus" + housename).style.setProperty("--house-clr", colors[i]);
        }
      } else {
        alert("Gemeinsame Farbe konnte nicht gespeichert werden! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'set_house_colors_common?' + argument, true);
  xhttp.send();

  return false;
}

/* Single */
function colors_single_number_change(event) {
  var hausnummer = document.getElementById('colors_single_number').value - 1;
  if (hausnummer > 23)
    return;

  Start_Loading();

  var argument = 'id=' + hausnummer.toString();

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        document.getElementById('colors_single_color').value = '#' + this.responseText.substring(2);
      } else {
        alert("Hausfarbe konnte nicht geladen werden! " + this.responseText);
      }

      End_Loading();
    }
  };
  xhttp.open('GET', 'get_house_color?' + argument, true);
  xhttp.send();
}

function color_single_save(event) {
  Start_Loading();
  var hausnummer = document.getElementById('colors_single_number').value - 1;

  var argument = 'id=' + hausnummer.toString();
  argument += '&color=' + document.getElementById('colors_single_color').value.substring(1);

  var xhttp = new XMLHttpRequest();
  xhttp.house_id = hausnummer;
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {
        var housename = "";
        if (this.house_id + 1 < 10)
          housename += "0";
        housename += (this.house_id + 1).toString();

        document.getElementById("Haus" + housename).style.setProperty("--house-clr", colors[this.house_id]);
      } else {
        alert("Farbe konnte nicht aktiviert werden! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'set_house_colors_single?' + argument, true);
  xhttp.send();

  return false;
}

function colors_general_brightness_changed(event) {
  var value = document.getElementById('colors_general_brightness_value').value;
  value = Math.round(value * 100 / 255);
  document.getElementById('colors_general_brightness_label').innerHTML = ' (' + value + ' %)';
}

function save_color_configuration(event) {
  Start_Loading();
  var argument = 'brightness=' + document.getElementById('colors_general_brightness_value').value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') { } else {
        alert("Konfiguration der Farben konnte nicht gespeichert werden! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'save_color_configuration?' + argument, true);
  xhttp.send();

  return false;
}


/* Configuration page */

function save_configuration(event) {
  Start_Loading();
  var switching_time_text = 'mode=' + document.getElementById('configuration_mode').value;
  switching_time_text += '&sylvester=' + (document.getElementById('configuration_sylvester').checked ? '1' : '0');
  switching_time_text += '&debug=' + (document.getElementById('configuration_debug').checked ? '1' : '0');

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        alert("Gespeichert");
      } else {
        alert("Speichern der Konfiguration fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'save_switch_configuration?' + switching_time_text, true);
  xhttp.send();

  return false;
}

function configuration_variable_Change() {
  var cv = document.getElementById("e_configuration_variable");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("e_cv_value").value = this.responseText;
    }
  };
  xhttp.open('GET', 'load_configuration_variable?name=' + cv.value, true);
  xhttp.send();
}

function save_configuration_variable(e) {
  Start_Loading();
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      alert(this.responseText);
    }
    End_Loading();
  };
  xhttp.open('GET', 'save_configuration_variable?name=' + encodeURIComponent(document.getElementById("e_configuration_variable").value) + '&value=' + encodeURIComponent(document.getElementById("e_cv_value").value), true);
  xhttp.send();
  return false;
}

function reset_configuration_variable(e) {
  if (confirm('Möchtest du die Einstellung wirklich auf Werkseinstellungen zurücksetzen?')) {
    Start_Loading();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200)
        alert(this.responseText);
      End_Loading();
    }
    xhttp.open('GET', 'reset?id=' + document.getElementById('e_configuration_variable').value, true);
    xhttp.send();
  }
  return false;
}

function reboot() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200)
      window.location.href = window.location.href;
  }
  xhttp.open('GET', 'reboot', true);
  xhttp.send();
  alert("Reboote ESP...");
  return false;
}

function reset_esp(event, wifi_reset) {
  if (confirm('Möchtest du alle Einstellungen wirklich auf Werkseinstellungen zurücksetzen?')) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200)
        window.location.href = window.location.href;
    }
    xhttp.open('GET', 'reset' + (wifi_reset ? '?wifi=true' : ''), true);
    xhttp.send();
    alert("ESP wird auf Werkseinstellungen zurückgesetzt!");
  }
  return false;
}

// House Moving
var last_parent;
var house;
var house_movement_active = false;

function toggle_house_movement() {
  house_movement_active = !house_movement_active;
  if (house_movement_active)
    document.getElementById("house_toggler").setAttribute('value', 'Häuser verschieben (aktiv)');
  else
    document.getElementById("house_toggler").setAttribute('value', 'Häuser verschieben');

  return false;
}

function allowDrop(ev) {
  if (!house_movement_active)
    return;
  ev.preventDefault();
}

function drag(ev) {
  if (!house_movement_active)
    return;
  house = ev.target;
  last_parent = document.getElementById(ev.target.id).parentNode;
}

function drop(ev) {
  if (!house_movement_active)
    return;
  ev.preventDefault();
  var target = ev.target;
  while (!target.classList.contains('target'))
    target = target.parentNode;
  document.getElementById('house_ids').value += last_parent.id.substring(4) + target.id.substring(4);
  last_parent.appendChild(target.getElementsByClassName('house')[0]);
  target.appendChild(house);
}


/* Switch */

function switch_number_change(event) {
  Start_Loading();
  var cv = document.getElementById("switch_number");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        //this.responseText: 0 NAME[20];type;pin;pin2;fun[0];fun[1];...;fun[5]
        var nextsemi = this.responseText.indexOf(';');
        document.getElementById('switch_name').value = this.responseText.substring(2, nextsemi);

        var lastsemi = nextsemi;
        nextsemi = this.responseText.indexOf(';', nextsemi + 1);
        var type = this.responseText.substring(lastsemi + 1, nextsemi);
        document.getElementById('switch_type').value = type;

        lastsemi = nextsemi;
        nextsemi = this.responseText.indexOf(';', nextsemi + 1);
        document.getElementById('switch_pin').value = this.responseText.substring(lastsemi + 1, nextsemi);

        lastsemi = nextsemi;
        nextsemi = this.responseText.indexOf(';', nextsemi + 1);
        document.getElementById('switch_pin2').value = this.responseText.substring(lastsemi + 1, nextsemi);

        for (var i = 0; i < 6; i++) {
          lastsemi = nextsemi;
          nextsemi = this.responseText.indexOf(';', nextsemi + 1);
          document.getElementById('switch_fun_' + i).value = this.responseText.substring(lastsemi + 1, nextsemi);
        }

        type = type - '0';
        ActivateFormularInputs(type);
      } else {
        alert("Laden der Schalterkonfiguration fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'load_switch_config?id=' + cv.value, true);
  xhttp.send();
}

function switch_type_change(event) {
  ActivateFormularInputs(parseInt(document.getElementById('switch_type').value));
}

function ActivateFormularInputs(type) {
  var inputs_name = Array('switch_pin', 'switch_pin2', 'switch_fun_0', 'switch_fun_1', 'switch_fun_2', 'switch_fun_3', 'switch_fun_4', 'switch_fun_5');

  var inputs_disabled = Array(
    Array(true, true, true, true, true, true, true, true),        // Deactivated
    Array(false, true, false, false, false, false, false, false), // Button
    Array(false, true, false, false, true, true, true, true),     // Switch 2
    Array(false, false, false, false, false, true, true, true)    // Switch 3
  );

  var inputs_invisible = Array(
    Array(false, true, true, true, true, true, true, true),       // Deactivated
    Array(false, true, false, false, false, false, false, false), // Button
    Array(false, true, false, false, true, true, true, true),     // Switch 2
    Array(false, false, false, false, false, true, true, true)    // Switch 3
  );

  var inputs_fun_names = Array(
    Array("", "", "", "", "", ""),                                            // Deactivated
    Array("Runter", "Unten", "Hoch", "Oben", "Kurzer Klick", "Langer Klick"), // Button
    Array("Position 1", "Position 2", "", "", "", ""),                        // Switch 2
    Array("Position 1", "Position 2", "Position 3", "", "", "")               // Switch 3
  );

  for (var i = 0; i < inputs_name.length; i++) {
    document.getElementById(inputs_name[i]).disabled = inputs_disabled[type][i];
    if (inputs_invisible[type][i])
      document.getElementById(inputs_name[i] + '_row').classList.add('switch_invisible');
    else
      document.getElementById(inputs_name[i] + '_row').classList.remove('switch_invisible');

    if (i > 1)
      document.getElementById(inputs_name[i] + '_label').innerHTML = inputs_fun_names[type][i - 2];
  }
}

function save_switch(event) {
  Start_Loading();
  document.getElementById('switch_number').buffered_index = document.getElementById('switch_number').value;
  document.getElementById('switch_name').buffered_name = document.getElementById('switch_name').value;
  var parameters = 'id=' + document.getElementById('switch_number').value;
  parameters += '&name=' + document.getElementById('switch_name').value;
  parameters += '&type=' + document.getElementById('switch_type').value;
  parameters += '&pin=' + document.getElementById('switch_pin').value;
  parameters += '&pin2=' + document.getElementById('switch_pin2').value;
  parameters += '&fun0=' + document.getElementById('switch_fun_0').value;
  parameters += '&fun1=' + document.getElementById('switch_fun_1').value;
  parameters += '&fun2=' + document.getElementById('switch_fun_2').value;
  parameters += '&fun3=' + document.getElementById('switch_fun_3').value;
  parameters += '&fun4=' + document.getElementById('switch_fun_4').value;
  parameters += '&fun5=' + document.getElementById('switch_fun_5').value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        document.getElementById('switch_number').getElementsByTagName('option')[document.getElementById('switch_number').buffered_index = document.getElementById('switch_number').value].innerHTML = document.getElementById('switch_name').buffered_name;
        alert("Gespeichert");
      } else {
        alert("Speichern der Schalterkonfiguration fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'save_switch?' + parameters, true);
  xhttp.send();

  return false;
}

/* Windmill */
function liveweather_toggled(event) {
  var inputs_name = Array('windmill_city_id', 'windmill_app_id', 'windmill_wind_speed_trigger');
  var disabled = !document.getElementById('windmill_liveweather_active').checked;

  for (var i = 0; i < inputs_name.length; i++)
    document.getElementById(inputs_name[i]).disabled = disabled;
}

function save_windmill_configuration(event) {
  Start_Loading();
  var arguments = 'outputpin=' + document.getElementById('windmill_pin_windmill').value.toString();
  arguments += '&inputpin=' + document.getElementById('windmill_pin_sensor').value.toString();
  arguments += '&action_duration=' + document.getElementById('windmill_action_duration').value.toString();
  arguments += '&liveweather_active=' + (document.getElementById('windmill_liveweather_active').checked ? '1' : '0');
  arguments += '&app_id=' + document.getElementById('windmill_app_id').value.toString();
  arguments += '&city_id=' + document.getElementById('windmill_city_id').value.toString();
  arguments += '&wind_speed_trigger=' + document.getElementById('windmill_wind_speed_trigger').value.toString();
  arguments += '&windmill_mode=' + document.getElementById('windmill_mode').value.toString();
  var selector = document.querySelector('input[name="windmill_speed_mode"]:checked');
  if (!selector) {
    alert("Kein Geschwindigkeitsmodus ausgewählt!");
    return false;
  }
  arguments += '&speed_mode=' + selector.value.toString();
  arguments += '&sc1=' + document.getElementById('windmill_speed_const_value').value.toString();
  arguments += '&sf1=' + document.getElementById('windmill_speed_fun_min').value.toString();
  arguments += '&sf2=' + document.getElementById('windmill_speed_fun_max').value.toString();
  arguments += '&sf3=' + document.getElementById('windmill_speed_fun_frequency').value.toString();
  arguments += '&sl1=' + document.getElementById('windmill_speed_live_min').value.toString();
  arguments += '&sl2=' + document.getElementById('windmill_speed_live_max').value.toString();
  arguments += '&sl3=' + document.getElementById('windmill_speed_live_turnoff').value.toString();

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText[0] == '0') {// Success
        alert("Gespeichert");
      } else {
        alert("Speichern der Einstellungen der Windmühle fehlgeschlagen! " + this.responseText);
      }
      End_Loading();
    }
  };
  xhttp.open('GET', 'save_windmill_configuration?' + arguments, true);
  xhttp.send();

  return false;
}

function windmill_speed_mode_change(s) {
  if (s == '0')
    document.getElementById('windmill_speed_mode_const').checked = true;
  else if (s == '1')
    document.getElementById('windmill_speed_mode_fun').checked = true;
  else if (s == '2')
    document.getElementById('windmill_speed_mode_live').checked = true;

  var names = ['windmill_sm_const', 'windmill_sm_fun', 'windmill_sm_live'];

  var inputs = document.getElementById('windmill_speed_mode_table').getElementsByTagName('input');
  for (var i = 0; i < inputs.length; i++)
    if (inputs[i].type != 'radio')
      inputs[i].disabled = !inputs[i].classList.contains(names[s]);
}