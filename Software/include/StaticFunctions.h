/**
 * @file Static_Functions.h
 * @author Julian Neundorf
 * @brief Consists of functions which are usable in multiple other files
 * @version 0.1
 * @date 2021-08-29
 *
 * Remarks:
 */

#pragma once

#include <Arduino.h>

 /**
  * @brief Clears the serial input buffer
  */
static void ClearSerial() {
  while (Serial.read() != -1)
    yield();
}

/**
* @brief Reads a single char from Serial and returns it
*
* @param blocking If true, the function is waiting until at least one char is received
* @param clear If true, the Serial input buffer is cleared after reading the first char
* @retval First read char, if no char is send returns -1
*/
static const char ReadSerialChar(const bool blocking = true, const bool clear = true) {
  while (blocking && !Serial.available())
    yield();
  const char c = Serial.read();
  if (clear)
    ClearSerial();
  return c;
}

/**
 * @brief Reads a line of characters from the serial.
 *
 * It reads until either the buffer is full, a CR or a LF is reached
 *
 * @tparam bufLength The length of the buffer.
 * @param buffer The buffer which will be filled with the read characters.
 * @retval The number of characters read.
 */
template<std::size_t bufLength>
uint8_t ReadLine(char (&buffer)[bufLength]) {
  uint8_t index = 0;
  while (index < bufLength - 1) {
    if (Serial.available()) {
      const int readChar = Serial.read();
      if (readChar != -1) {
        if (readChar == '\n' || readChar == '\r') {
          buffer[index] = '\0';
          break;
        }
        buffer[index++] = readChar;
      }
    }
    taskYIELD();
  }
  return index;
}

/**
 * @brief Returns whether offset and length are valid for the given string.
 *
 * @param str the string from which the length is used.
 * @param offset the offset from the beginning of the string. Must not be negative.
 * @param length the length of the selected text. If zero, it'll be set to the remaining length.
 * @retval whether offset + length are not longer than the string.
 */
static const bool validRange(const String& str, const unsigned int offset, unsigned int& length) {
  // when offset + length < offset it means, both are to large and overflow
  if (offset + length < offset || offset + length > str.length()) {
    return false;
  }
  if (length == 0) {
    length = str.length() - offset;
  }
  return true;
}

static const bool isNumber(const String& str, const unsigned int offset = 0, unsigned int length = 0) {
  if (!validRange(str, offset, length)) {
    return false;
  }
  const char* c_str = str.c_str() + offset;
  while (length-- > 0) {
    if (*c_str < '0' || *c_str > '9') {
      return false;
    }
    c_str++;
  }
  return true;
}

/**
 * @brief Converts a String to T
 *
 * @param str String with the integer
 * @param offset the offset from the beginning of the string. Must not be negative.
 * @param length the length of the selected text. If zero, it'll be set to the remaining length.
 * @retval Converted integer
 */
template<typename T>
static const T parseAsInt(const String& str, const unsigned int offset = 0, unsigned int digits = 0) {
  if (!validRange(str, offset, digits)) {
    return 0;
  }
  T val = 0;
  const char* c_str = str.c_str() + offset;
  while (digits-- > 0) {
    val = val * 10 + (*c_str - '0');
    c_str++;
  }
  return val;
}

constexpr uint8_t bytesPerColor = 3;
constexpr uint8_t hexCharsPerByte = 2;

/**
 * @brief Converts a hex-string into an array of uint8_t-values
 *
 * @tparam N the number of bytes it'll write
 * @param str the hex string which need to consist of at least 2*N characters of 0-9 and a-f
 * @param value the uint8_t array which will be filled with the values
 */
template<std::size_t N>
static void parseAsHex(const char* str, uint8_t (&value)[N]) {
  uint8_t byteVal = 0;
  for (uint8_t i = 0; i < N * hexCharsPerByte; i++) {
    byteVal += *str >= '0' && *str <= '9' ? *str - '0' : (*str >= 'a' && *str <= 'f' ? *str - 'a' + 10 : 0);
    if (i % 2 == 1) {
      value[i / 2] = byteVal;
    }
    byteVal <<= 4;
    str++;
  }
}

/**
 * @brief Converts a hex-string of length 6 into an CRGB value.
 *
 * The value will be read "RRGGBB".
 *
 * @param str the hex string with a length of 6 between 0-9 and a-f
 * @retval the converted rgb value.
 */
static const CRGB parseAsColor(const String& str) {
  CRGB color = {0, 0, 0};
  if (str.length() == bytesPerColor * hexCharsPerByte) {
    const char* c_str = str.c_str();
    parseAsHex(c_str, color.raw);
  }
  return color;
}

/**
 * @brief Converts a String to a float
 *
 * @param str String with the integer
 * @retval float Converted integer
 */
static float StringToFloat(String str) {
  float val = 0;
  uint8_t i = 0;
  for (; i < str.length() && str[i] != '.' && str[i] != ','; i++)
    val = val * 10 + (str[i] - '0');

  i++;
  for (float position = 0.1; i < str.length(); i++, position /= 10)
    val += position * (str[i] - '0');
  return val;
}