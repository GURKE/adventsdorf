/**
 * @file Switch.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-06
 *
 * Remarks:
 */

#pragma once

#include <stdint.h>
#include "Storage.h"
#include <array>

struct Switches_Storage {
public:
  /**
   * @brief Type of button
  */
  enum Type : uint8_t {
    /**
     * @brief Button not configured
     */
    None = 0,

    /**
     * @brief Monostable switch with one self-resetting position
    */
    Button = 1,

    /**
     * @brief Bistable switch with two possible positions
    */
    Switch2 = 2,

    /**
     * @brief Tristable switch with three possible positions
    */
    Switch3 = 3
  };

  // Amount of configurable functions of each switch (Pressing, Pressed, High, Bottom, Short click, Long click, etc.)
  static const uint8_t AM_SWITCH_FUNS = 6;

  // Amount of chars of a single switch name
  static const uint8_t STRLENGTH = 20;

  /**
   * @brief Construct a new Switches_Storage::Switches_Storage object
  */
  Switches_Storage();

protected:
  Type type;

  uint8_t pin;
  uint8_t pin2;

  uint8_t fun[AM_SWITCH_FUNS];
  char name[STRLENGTH];
};

class Switches {
public:
  static constexpr uint8_t AM_Switches = 10;

  static constexpr uint8_t AM_AVAILABLE_SWITCH_FUNS = 11;

  Switches(void(*_fun[])());

  void Load();

  const std::array<const char*, Switches::AM_Switches> GetNames() const;

  bool GetConfiguration(uint8_t _index, const char** _name, uint8_t* _type, uint8_t* _pin, uint8_t* _pin2, const uint8_t** _fun) const;

  bool SetConfiguration(uint8_t _id, const char* _name, uint8_t type, uint8_t pin, uint8_t pin2, uint8_t* fun);

  void Reset();

  enum Functions {
    None = 0,
    Set_Mode_Off,
    Set_Mode_Auto,
    Set_Mode_On,
    Button_Clicked_Short,
    Button_Clicked_Long,
    Button_Pressing,
    Button_Pressed,
    Button_Lifting,
    Button_Lifted,
    Switch_Pos0,
    Switch_Pos1,
    Switch_Pos2,
  };

  class Switch : public Switches_Storage {
  public:
    /**
     * @brief Construct a new Switch object
    */
    Switch();

    /**
     * @brief Construct a new Switch object
     *
     * @param _ss
    */
    Switch(Switches_Storage& _ss);

    /**
     * @brief Checks the current button status and calls associated function
     */
    void Check_Status();

    /**
     * @brief Returns the switch's name
     *
     * @retval const char* name
    */
    const char* GetName() const;

    /**
     * @brief Returns the Configuration object
     *
     * @param _name
     * @param _type
     * @param _pin
     * @param _pin2
     * @param _fun
    */
    void GetConfiguration(const char** _name, uint8_t* _type, uint8_t* _pin, uint8_t* _pin2, const uint8_t** _fun) const;

    bool SetConfiguration(uint8_t _id, const char* _name, uint8_t _type, uint8_t _pin, uint8_t _pin2, uint8_t* _fun);

  private:
    void run_fun(uint8_t _index);

    uint32_t down_time;

    enum class Event_Type {
      Pressed,
      Lifted,
      Pos0,
      Pos1,
      Pos2
    } event_type;

    class Functions {
    public:
      class Button {
      public:
        static const uint8_t pressing = 0;
        static const uint8_t pressed = 1;
        static const uint8_t lifting = 2;
        static const uint8_t lifted = 3;
        static const uint8_t clicked_short = 4;
        static const uint8_t clicked_long = 5;
      };

      class Switch2 {
      public:
        static const uint8_t position0 = 0;
        static const uint8_t position1 = 1;
      };

      class Switch3 {
      public:
        static const uint8_t position0 = 0;
        static const uint8_t position1 = 1;
        static const uint8_t position2 = 2;
      };
    };

    void Check_Button();
    void Check_Switch2();
    void Check_Switch3();
  };

private:
  static void Runner_static(void* _this);
  void Runner();

  Switch switches[AM_Switches];
};