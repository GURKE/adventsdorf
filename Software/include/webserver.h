#pragma once

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include "Storage.h"
#include <ESPAsyncWebServer.h> // https://github.com/me-no-dev/ESPAsyncWebServer/archive/master.zip und https://github.com/me-no-dev/AsyncTCP/archive/master.zip
#include "Switching_Times.h"
#include "LED.h"
#include "Switch.h"

#define WIFI_AP_SSID "Adventsdorf"
#define WIFI_AP_PASS "3.141592"

class WIFI {
public:
  WIFI();
  void Check_Wifi();
  static uint8_t GetSignalStrengthPercentage();

private:
  // Handles
  static void HandleRoot(AsyncWebServerRequest* request);
  static String HandleRoot_Placeholder(const String& var);
  static void HandleNotFound(AsyncWebServerRequest* request);

  // Lamptests
  static void Handle_CompleteLampTest(AsyncWebServerRequest* request);
  static void Handle_CycleLampTestTime(AsyncWebServerRequest* request);
  static void Handle_CycleLampTestSwitch(AsyncWebServerRequest* request);
  static void Handle_SingleTest(AsyncWebServerRequest* request);
  static void Handle_StopTest(AsyncWebServerRequest* request);

  // Switching Times
  static void Handle_SetSwitchingConfiguration(AsyncWebServerRequest* request);

  static void Handle_GetSwitchingTimes(AsyncWebServerRequest* request);
  static void Handle_SetSwitchingTimes(AsyncWebServerRequest* request);

  // House Colors
  static void Handle_TryColor(AsyncWebServerRequest* request);
  static void Handle_SetHouseColorsCommon(AsyncWebServerRequest* request);
  static void Handle_SetHouseColorsSingle(AsyncWebServerRequest* request);
  static void Handle_GetHouseColor(AsyncWebServerRequest* request);
  static void Handle_SetColorConfigurations(AsyncWebServerRequest* request);
  static void Handle_ResetHouseColors(AsyncWebServerRequest* request);

  // Configurations  
  static void Handle_NewHouseOrdner(AsyncWebServerRequest* request);
  static void Handle_ResetHousePos(AsyncWebServerRequest* request);

  static void Handle_LoadConfigurationVariable(AsyncWebServerRequest* request);
  static void Handle_SaveConfigurationVariable(AsyncWebServerRequest* request);

  static void Handle_Reset(AsyncWebServerRequest* request);

  // Switches
  static void Handle_LoadSwitchConfiguration(AsyncWebServerRequest* request);
  static void Handle_SaveSwitch(AsyncWebServerRequest* request);

  // Windmill
  static void Handle_SaveWindmillConfiguration(AsyncWebServerRequest* request);

  // Other functions
  static void CalcSignalStrength(void* pvParameters);
  static void OnWiFiEvent(WiFiEvent_t event);
  static void NetworkConnected();
};