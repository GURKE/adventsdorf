/**
 * @file Storage.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-08-31
 *
 * Remarks:
*/

#pragma once

#include <stdint.h>
#include <Arduino.h>

/**
 * @brief Function called after resetting switching times configuration
*/
extern void Reset_SwitchingTimes();

/**
 * @brief Function called after resetting led control configuration
*/
extern void Reset_LED_Control();

/**
 * @brief Function called after resetting switch configuration
*/
extern void Reset_Switch();

class Storage {
public:
  static constexpr uint16_t EEPROM_SIZE = 1024;
  static constexpr uint16_t OBJECTS_COUNT = 9;

  static constexpr uint16_t LENGTH_PER_SWITCH = 29;
  static constexpr uint16_t LENGTH_WINDMILL = 39;

  /**
   * @brief
   *
  */
  class Object {
  public:
    /**
     * @brief Type of the content of the object
    */
    enum class Type {
      Text,
      Bytes,
      Struct
    };


    /**
     * @brief Type of the default values of the object
    */
    enum class Default_Type {
      Zero,
      Max,
      Rising_Start_Zero,
      Rising_Start_One,
      Individual
    };


    /**
     * @brief Construct a new storring object
     *
     * @param _length Length in bytes
     * @param _name Name
     * @param _type Type of the value (Type::*)
     * @param _default_type Type of the default value (Default_Type::*)
     * @param _reset_fun A function called after resetting
    */
    Object(uint16_t _length, const char* _name, Type _type = Type::Bytes, Default_Type _default_type = Default_Type::Zero, void (*_reset_fun)() = nullptr);


    /**
     * @brief Initializes the objects startaddress and loads its value. Function should not be called outside of Storage.cpp
     *
     * @param _start Startaddress in EEPROM
    */
    void Init(uint16_t _start);


    /**
     * @brief Saves a value into EEPROM
     *
     * @param value String value to be saved
     * @retval bool Return 0 if success, 1 if address is out of range
    */
    bool Save(String value);

    /**
     * @brief Saves a value into EEPROM
     *
     * @param _value One Byte value to be saved
     * @retval bool Return 0 if success, 1 if address is out of range
    */
    bool Save(uint8_t value);

    /**
     * @brief Saves an object into EEPROM
     *
     * @param _value Object to be saved
     * @param _length Length of the value pointer
     * @param _offset Amount of bytes the start is shifted from the address
     * @retval bool Return 0 if success, 1 if address is out of range
    */
    bool Save(const void* _value, uint16_t _length = -1, const uint16_t _offset = 0);

    /**
     * @brief Saves a value into EEPROM
     *
     * @param value uint8_t pointer to be saved
     * @param _length Length of the value pointer
     * @param _offset Amount of bytes the start is shifted from the address
     * @retval bool Return 0 if success, 1 if address is out of range
    */
    bool Save(const char* value, uint16_t _length = -1, const uint16_t _offset = 0);

    /**
     * @brief Saves a value into EEPROM
     *
     * @param value uint8_t pointer to be saved
     * @param _length Length of the value pointer
     * @param _offset Amount of bytes the start is shifted from the address
     * @retval bool Return 0 if success, 1 if address is out of range
    */
    bool Save(const uint8_t* value, uint16_t _length = -1, const uint16_t _offset = 0);


    /**
     * @brief Casts object into its value's address
     *
     * @retval const uint8_t* Address of the first value
    */
    operator const uint8_t* () const { return value; }

    /**
     * @brief Casts object into its value's address
     *
     * @retval const char* Address of the first value
    */
    operator const char* () const { return (const char*)value; }

    /**
     * @brief Casts object into its value's address
     *
     * @retval const void* Address of the first value
    */
    operator const void* () const { return (const void*)value; }

    /**
     * @brief Casts object into its value
     *
     * @retval uint8_t Value of the first byte
    */
    operator uint8_t() const { return *value; }


    /**
     * @brief Resets the object to its default value
    */
    void Reset();


    /**
     * @brief Get the length of the requested object
     *
     * @retval uint16_t Length of the requested object
    */
    uint16_t GetLength() const;

    /**
     * @brief Get the name of the object
     *
     * @retval const char* Name of the object
    */
    const char* GetName() const;

    /**
     * @brief Get the type of the requested object
     *
     * @retval Type Type of the requested object
    */
    Type GetType() const;

  private:
    const uint16_t length;
    const char* name;
    Type type;
    uint16_t start;
    const Default_Type default_type;
    uint8_t* const value;
    void(*reset_fun)();
  };

private:
  Object objects[OBJECTS_COUNT] = {
    Object(32, "WIFI SSID", Object::Type::Text),                                                                //   0, 0x000, WIFI SSID
    Object(63, "WIFI Passwort", Object::Type::Text),                                                            //  32, 0x020, WIFI Passwort
    Object(24, "House Positions", Object::Type::Bytes, Object::Default_Type::Rising_Start_Zero),                //  95, 0x05F, Haus Positionen
    Object(120, "Schaltzeiten", Object::Type::Bytes, Object::Default_Type::Zero, Reset_SwitchingTimes),         // 119, 0x077, 20 Schaltzeiten, HH:mm - HH:mm, Wochentage, Aktiv
    Object(1, "Schaltzeit Konfigurationen"),                                                                    // 239, 0x0EF, Schaltzeit Konfigurationen, 0b0000 00 Sylvester Enabled
    Object(24 * 3, "Hausfarben", Object::Type::Bytes, Object::Default_Type::Max, Reset_LED_Control),            // 240, 0x0F0, Hausfarben (24 x RGB)
    Object(1, "Helligkeit", Object::Type::Bytes, Object::Default_Type::Max),                                    // 312, 0x138, Helligkeit
    Object(10 * LENGTH_PER_SWITCH, "Schalter", Object::Type::Struct, Object::Default_Type::Zero, Reset_Switch), // 313, 0x139, Schalterkonfigurationen 20x (type, pin, pin2, fun{6}, name{20})
    Object(LENGTH_WINDMILL, "Windmuehle", Object::Type::Struct)                                                 // 603, 0x25B, Windmühlen Konfigurationen (outputpin, inputpin, action_duration{2}, Weather_api_id{16}, Weather_city_id{4}, wind_speed_trigger, weather_enabled, mode, speedmode, sm_constant_speed, sm_function_min, sm_function_max, sm_function_frequency, sm_live_min, sm_live_max, sm_live_turnoff)
    // 
  };

public:
  Object* const WiFi_SSID = objects + 0;
  Object* const WiFi_PASS = objects + 1;
  Object* const House_Positions = objects + 2;
  Object* const Switching_Times = objects + 3;
  Object* const Switching_Configurations = objects + 4;
  Object* const House_Colors = objects + 5;
  Object* const Brightness = objects + 6;
  Object* const Switches = objects + 7;
  Object* const Windmill = objects + 8;

  /**
    * @brief Construct a new Storage object
    */
  Storage();

  /**
   * @brief Resets a specific EEPROM to 0
   *
   * @param index First removed object
   */
  void Reset(uint8_t index);

  /**
   * @brief Resets the whole EEPROM to 0
   *
   * @param wifi_incl Wether it will also remove the WiFi information (SSID and Password)
   */
  void ResetAll(bool wifi_incl);

  /**
   * @brief Get the name of the ith object
   *
   * @param index Index of the object
   * @retval const char* Object's name
  */
  const char* GetObjectsName(uint16_t index);

  /**
   * @brief Get the ith object
   *
   * @param index Index of the object
   * @retval const Object* Requested object
  */
  Object* GetObject(uint16_t index);
};