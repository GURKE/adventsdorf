/**
 * @file Classes.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-12
 *
 * Remarks:
*/
#pragma once

#include "webserver.h"
#include "Storage.h"
#include "Switching_Times.h"
#include "LED.h"
#include "Switch.h"
#include "Windmill.h"
#include "version.h"

#ifndef VERSION
#pragma message "No version defined!"
#define VERSION "Unknown version"
#endif

extern WIFI* wifi;
extern Storage* storage;
extern SwitchingTimes* switchingtimes;
extern LED_Control* LED_control;
extern Switches* switches;
extern Windmill* windmill;

extern struct tm main_time;

void Init();
bool CheckTime();