/**
 * @file Switching_Times.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-08-31
 *
 * Remarks:
*/

#pragma once

#include <stdint.h>
#include "Storage.h"
#include <FastLED.h>

/**
 * @brief Class controlling all switching times
 */
class SwitchingTimes {
public:
  /**
   * @brief Class storing hours and minutes
   */
  class Time {
  public:
    Time();
    Time(const uint8_t _hour, const uint8_t _minute);
    Time(const String& _time);
    uint8_t Hour;
    uint8_t Minute;
  };

  /**
   * @brief Construct a new Switching Times object
   */
  SwitchingTimes();

  /**
   * @brief Loads times from EEPROM
  */
  void Load();

  /**
   * @brief Get all configurations of one switching Time object
   *
   * @param index The index of the switching time object
   * @param activate A Time pointer returning the activation time
   * @param deactivate A Time pointer returning the deactivation time
   * @param weekdays A pointer returning on which weekday this switching time will be activated (Format: 0bxSSFTWTM)
   * @param enabled A pointer storing if the switching time is activated
   * @retval Returns false if index is out of size
  */
  bool GetSwitchingTime(uint8_t index, Time* activate, Time* deactivate, uint8_t* weekdays, bool* enabled) const;

  /**
   * @brief Set all configurations of one switching Time object
   *
   * @param index The index of the switching time object
   * @param activate Activation time of format "HH:mm"
   * @param deactivate Deactivation time of format "HH:mm"
   * @param weekdays Storing on which weekday the switching time will be activated (Format: 0bxSSFTWTM)
   * @param enabled Storing if the switching time is enabled
   * @retval Returns false if index is out of size
   */
  bool SetSwitchingTime(uint8_t index, String activate, String deactivate, uint8_t weekdays, bool enabled);

  // Maximum amount of available switching times
  static const uint8_t MAX_SWICHTING_TIMES = 20;

  /**
   * @brief Checks if a switching time is active
   *
   * @retval Returns true if at least one switching time is active
   */
  bool CheckTime() const;

  /**
   * @brief
   */
  class SwitchingTime {
  public:
    SwitchingTime();
    SwitchingTime(uint8_t _activate_Hour, uint8_t _activate_Minute, uint8_t _deactivate_Hour, uint8_t _deactivate_Minute, uint8_t _weekdays, bool _enabled = true);

    Time time_activate;
    Time time_deactivate;
    uint8_t weekdays;
    bool enabled;
  };

private:
  // List of all switching times
  SwitchingTimes::SwitchingTime times[SwitchingTimes::MAX_SWICHTING_TIMES];
};