/**
 * @file LED.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-08-31
 *
 * Remarks:
 */

#pragma once

#include <FastLED.h>
#include "Storage.h"

enum class Lampteststatus {
  Off,
  Single,
  CycleTime,
  CycleSwitch,
  Complete
};

class LED_Control {
public:
  // Outputpin for the stripe
  static const uint8_t LED_PIN = 19;

  // Amount of LEDs
  static const uint16_t NUM_LEDS = 24;

  // Duration during lamp test of a single light in milliseconds
  static const uint32_t TEST_DURATION = 3000;

  /**
   * @brief Construct a new led control object
   */
  LED_Control();

  /**
   * @brief (De-)activates lamptest
   *
   * @param nexttype The requested lampstatus (Lampteststatus::Off, Single, CycleTime, CycleSwitch, Complete)
   * @param new_testlamp Zero based index of the lamp to be tested, only neccessary for nexttype == Single
   */
  void SwitchLampTest(const Lampteststatus NextType, const uint16_t lamp = -1, const CRGB* color = nullptr);

  /**
   * @brief Returns the current test status
   * @retval Lampteststatus
   */
  Lampteststatus Getlamptesttype() const;

  /**
   * @brief Returns if the light of the given zero based index is on
   *
   * @param index Zero based index of the lamp
   * @retval true Light is on
   * @retval false Light is off
   */
  bool GetLighton(const uint16_t index) const;


  // Configurations
  enum Mode {
    Off = 0,
    Auto = 1,
    On = 2
  };

  /**
   * @brief Get mode (Off, Auto, On)
   *
   * @param _mode
   */
  void SetMode(const Mode _mode);

  void SetMode(const uint8_t _mode);

  Mode GetMode() const;

  void SetSylvester(const bool _sylvester = true);
  bool GetSylvester() const;

  void SetDebug(const bool _debug = true);
  bool GetDebug() const;

  void LoadColors();
  static void CheckLEDs_static(void* _this);
  void CheckLEDs();

  void CheckSylvester();
  void CalculateAdventLight();

  CRGB* GetColors();
  const CRGB* GetHouseColors();

  bool* GetSwitchingTimesStatus() const;
  void SaveConfiguration();

  void SetBrightness(uint8_t scale);
private:
  void SetColor(CRGB* target, const CRGB source);
  void SetColor(const uint16_t index);
  void SetColor(const uint16_t target, const CRGB source);
  void SetColor(const uint16_t target, const CRGB* color);
  void SetColor(const uint16_t target, const uint8_t R, const uint8_t G, const uint8_t B);
  void RemoveColor(const uint16_t target);
  void RemoveColorAll();

  void Apply();

  const CRGB* GetColor(const uint16_t index) const;
  CRGB* GetColor(const uint16_t index);

  uint32_t last_run = 0;

  Lampteststatus lampstatus = Lampteststatus::Off;
  uint16_t testlamp = -1;

  // Colors of houses stored in the EEPROM
  CRGB house_colors[NUM_LEDS];

  // "Live" LED output buffer
  CRGB leds[NUM_LEDS];

  CRGB ledBuffer[NUM_LEDS];

  /* Order of the soldered LEDs. The first key means the first house number and stores as value the led position.
   * Example:
   *  LED_ORDER[5] = 7 -> House with number 4 is the 8th LED in the row.
  */
  const uint8_t LED_ORDER[NUM_LEDS] =
  { 18, 4, 0, 9, 2, 1, 12, 20, 6, 3, 23, 7, 10, 11, 17, 5, 14, 8, 19, 21, 16, 22, 13, 15 };
  //HN 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };

  bool Houses_On[NUM_LEDS];

  // Handler for swichting lights during test
  TaskHandle_t Light_Switcher;

  // Configurations
  Mode mode;
  bool sylvester;
  bool debug;
};