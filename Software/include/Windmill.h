/**
 * @file Windmill.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-14
 *
 * Remarks:
*/
#pragma once

#include "stdint.h"

#pragma pack(push, 1)
struct Windmill_Storage {
  // Pin of the windmill
  uint8_t outputpin;

  // Pin of the motion detector
  uint8_t inputpin;

  uint16_t action_duration;

  uint8_t weather_app_id_raw[16];

  uint32_t weather_city_id_raw;

  // Least windspeed in m/s at which the mill starts to rotate when triggered by motion
  uint8_t weather_wind_speed_trigger : 4;

  // Weather controlled windmill
  uint8_t weather_enabled : 1;

  // Action mode
  enum class Mode : uint8_t { Off, MotionSwitchtime, Motion, On } mode = Mode::Off;

  // Speed mode
  enum class Speedmode : uint8_t { Const, Function, Live } speed_mode = Speedmode::Const;

  // Configured rotation speed (if speed_mode == Speedmode::Const)
  uint8_t sm_constant_speed;

  // Minimum rotation speed (if speed_mode == Speedmode::Function)
  uint8_t sm_function_min;

  // Maximum rotation speed (if speed_mode == Speedmode::Function)
  uint8_t sm_function_max;

  // Frequency rotation speed function (if speed_mode == Speedmode::Function)
  uint16_t sm_function_frequency;

  // Minimum rotation speed (if speed_mode == Speedmode::Live)
  uint8_t sm_live_min;

  // Maximum rotation speed (if speed_mode == Speedmode::Live)
  uint8_t sm_live_max;

  // Turn off real speed (if speed_mode == Speedmode::Live)
  float sm_live_turnoff;
};
#pragma pack(pop)

class Windmill : private Windmill_Storage {

public:
  /**
   * @brief Construct a new Windmill
  */
  Windmill();

  /**
   * @brief Returns if the live weather feature is active
   *
   * @retval True, if live weather is used
  */
  bool GetLiveWeatherActive() const;

  /**
   * @brief Return the trigger windspeed in m/s
   *
   * @retval uint8_t windspeed in m/s
  */
  uint8_t GetWindSpeedTrigger() const;

  /**
   * @brief Get the city id defined by openweathermap.org
   *
   * @retval const char* city id
  */
  const char* GetCityID() const;

  /**
   * @brief Get the appid defined by openweathermap.org
   *
   * @retval const char* app id
  */
  const char* GetAppID() const;

  /**
   * @brief Get the pin of the windmill
   *
   * @retval uint8_t Pin of windmill
  */
  uint8_t GetPinWindmill() const;

  /**
   * @brief Get the pin of the motion detector
   *
   * @retval uint8_t Pin of motion detector
  */
  uint8_t GetPinMotiondetector() const;

  /**
   * @brief Get duration of a windmill action in seconds
   *
   * @retval uint16_t Duration of a windmill action in seconds
  */
  uint16_t GetActionduration() const;

  /**
   * @brief Set the Configuration object
   *
   * @param _outputpin Outputpin for the windmill
   * @param _inputpin Inputput for the motion detector
   * @param _action_duration Action duration in seconds after triggering
   * @param _weather_enabled Enable triggering by live wind
   * @param _city_id City id of openweathermap.org for live wind
   * @param _app_id City id of openweathermap.org for live wind
   * @param _wind_trigger Least wind speed the windmill starts to run
   * @param _mode Mode (Off, Auto, On)
   * @param _speed_mode Speedmode (Const, Function, Live)
   * @param _constant_speed Rotation speed [1, 255]
   * @param _fun_min Function mode minimum speed [1, 255]
   * @param _fun_max Function mode maximum speed [1, 255]
   * @param _fun_freq Function mode frequency [1, 65535]
   * @param _live_min Live mode minimum speed [1, 255]
   * @param _live_max Live mode maximum speed [1, 255]
   * @param _live_turn_off Live mode Turn off speed
   * @retval Returns true if configuration could saved correctly
  */
  bool SetConfiguration(uint8_t _outputpin, uint8_t _inputpin, uint16_t _action_duration, bool _weather_enabled, const char* _app_id, const char* _city_id, uint8_t _weather_wind_speed_trigger, uint8_t _mode, uint8_t _speed_mode, uint8_t _constant_speed, uint8_t _fun_min, uint8_t _fun_max, uint16_t _fun_freq, uint8_t _live_min, uint8_t _live_max, float _live_turn_off);

  /**
   * @brief Switches windmill on
  */
  void change_mode_on();

  /**
   * @brief Switches windmill off
  */
  void change_mode_off();

  /**
   * @brief Switches windmill on, when lights are on and motion
  */
  void change_mode_motionswitchtime();

  /**
   * @brief Switches windmill on, when there is motion
  */
  void change_mode_motion();

  /**
   * @brief Get the Mode (Off, Auto, On)
   * @retval Mode
  */
  Mode getMode() const;

  /**
   * @brief Get the Speed Mode (Const, Function, Live)
   * @return Speed Mode
   */
  Speedmode getSpeedMode() const;

  /**
   * @brief return Rotation speed (if speed_mode == Speedmode::Const) [1, 255]
   */
  uint8_t Get_Constant_Speed() const;

  /**
   * @brief return Minimum rotation speed (if speed_mode == Speedmode::Function) [1, 255]
   */
  uint8_t Get_Function_Min() const;

  /**
   * @brief return Maximum rotation speed (if speed_mode == Speedmode::Function) [1, 255]
   */
  uint8_t Get_Function_Max() const;

  /**
   * @brief return Frequency rotation speed (if speed_mode == Speedmode::Function) [1, 255]
   */
  uint16_t Get_Function_Frequency() const;

  /**
   * @brief return Minimum rotation speed (if speed_mode == Speedmode::Live) [1, 255]
   */
  uint8_t Get_Live_Min() const;

  /**
   * @brief return Maximum rotation speed (if speed_mode == Speedmode::Live) [1, 255]
  */
  uint8_t Get_Live_Max() const;

  /**
   * @brief return Turn off real speed (if speed_mode == Speedmode::Live) [1, 255]
  */
  float Get_Live_Turnoff() const;

  /**
   * @brief return whether it is currently running.
   */
  bool running() const;

private:
  /**
   * @brief Caller function for Runner() to get called by xTask
   *
   * @param _this The windmill-object
  */
  static void Runner_static(void* _this);

  /**
   * @brief Checks motion detection and de-/activates windmill
  */
  void Runner();

  /**
   * @brief Get windspeed from internet
   * @return true, if loading was successfull
   */
  bool GetRealWindSpeed();

  /**
   * @brief Calculate the new windmill speed
   * @return uint8_t Windmillspeed [0, 255]
   */
  uint8_t CalculateSpeed();

  // Windmill on/off in auto mode
  bool auto_on = false;

  // The last motion triggered activation of the windmill in milli seconds
  unsigned long last_motion_triggered_time = 0;

  // Time in ms when next time the weather will be downloaded
  uint32_t next_weather_check = 0;

  // Time in ms between two weather downloads
  const uint32_t WEATHER_CHECK_WAITING_TIME = 60000;

  char url[118] = "http://api.openweathermap.org/data/2.5/weather?id=*******&appid=********************************&units=metric&lang=de";
  char* weather_app_id;
  char* weather_city_id;

  // Windspeed in m/s
  uint8_t weather_wind_speed;
};